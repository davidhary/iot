
namespace isr.IoT.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Initial State Tests";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Unit Tests for the Initial State Client Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "isr.Initial.State.Tests";
    }
}
