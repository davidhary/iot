﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.IoT.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.IoT.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.IoT.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
