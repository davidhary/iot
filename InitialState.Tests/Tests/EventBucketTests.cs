using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

using isr.IoT.InitialState;
using isr.IoT.InitialState.DispatcherExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.IoT.InitialStateTests
{

    /// <summary> Enum extensions tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-14 </para>
    /// </remarks>
    [TestClass()]
    public class EventBucketTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            AssignBucketBase( null );
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " EVENT ELEMENT CLASSES "

        /// <summary> A telemetry. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-04 </para>
        /// </remarks>
        internal class Telemetry
        {

            /// <summary> Gets the heading. </summary>
            /// <value> The heading. </value>
            public int Heading { get; set; }

            /// <summary> Gets the speed. </summary>
            /// <value> The speed. </value>
            public double Speed { get; set; }

            /// <summary> Gets the status. </summary>
            /// <value> The status. </value>
            public string Status => this.Heading > 270 ? "Q4" : this.Heading > 180 ? "Q3" : this.Heading > 90 ? "Q2" : "Q1";
        }

        #endregion

        #region " CLIENT "

        /// <summary> Reads access key. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The access key. </returns>
        private static string ReadAccessKey()
        {
            return My.MyProject.Computer.FileSystem.ReadAllText( My.Settings.Default.AccessKeyFileName );
        }

        /// <summary> Handles the exception event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Error event information. </param>
        private static void HandleExceptionEvent( object sender, System.IO.ErrorEventArgs e )
        {
            Assert.IsNotNull( e, $"{e.GetException()}" );
        }

        /// <summary> Gets or sets the bucket base. </summary>
        /// <value> The bucket base. </value>
        private static BucketBase BucketBase { get; set; }

        /// <summary> Assign bucket base. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="value"> The value. </param>
        private static void AssignBucketBase( BucketBase value )
        {
            if ( BucketBase is object )
            {
                BucketBase.ExceptionOccurred -= HandleExceptionEvent;
                BucketBase.Dispose();
                BucketBase = null;
            }

            BucketBase = value;
            if ( BucketBase is object )
            {
                BucketBase.ExceptionOccurred += HandleExceptionEvent;
            }
        }

        /// <summary> Creates epoch event bucket client. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The new epoch event bucket client. </returns>
        public static EpochEventBucket CreateEpochEventBucketClient()
        {
            string accessKey = ReadAccessKey();
            var eventClient = new EpochEventBucket( accessKey );
            AssignBucketBase( eventClient );
            return eventClient;
        }

        /// <summary> Creates ISO 8601 event bucket client. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The new ISO 8601 event bucket client. </returns>
        public static Iso8601EventBucket CreateIso8601EventBucketClient()
        {
            string accessKey = ReadAccessKey();
            var eventClient = new Iso8601EventBucket( accessKey );
            AssignBucketBase( eventClient );
            return eventClient;
        }

        /// <summary> Creates the client. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The new client. </returns>
        public static Client CreateClient()
        {
            string accessKey = ReadAccessKey();
            var client = new Client( accessKey );
            return client;
        }

        #endregion

        #region " CREATE BUCKET TESTS "

        /// <summary> Creates epoch event bucket. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        /// <param name="bucketKey">  The bucket key. </param>
        /// <param name="bucketName"> Name of the bucket. </param>
        /// <returns> The new epoch event bucket. </returns>
        public static EpochEventBucket CreateEpochEventBucket( string bucketKey, string bucketName )
        {
            try
            {
                var eventClient = CreateEpochEventBucketClient();
                var statusKeyTuple = eventClient.CreateBucket( bucketKey, bucketName );
                Assert.IsTrue( BucketBase.ExpectedCreateBucketStatusCodes().Contains( statusKeyTuple.Item1 ), $"{statusKeyTuple.Item1} is not expected" );
                Assert.AreEqual( bucketKey, statusKeyTuple.Item2, "bucket key should be set correctly" );
                return eventClient;
            }
            catch ( RestApiException ex )
            {
                throw new Exception( $"{ex.Message}. Details: {ex.ResponseContent}" );
            }
        }

        /// <summary> (Unit Test Method) tests create epoch event bucket. </summary>
        /// <remarks> Last tested: 2005-11-19 16:10. </remarks>
        [TestMethod]
        public void CreateEpochEventBucketTest()
        {
            string bucketKey = $"isrEpoch{DateTimeOffset.Now.Date:yyyyMMdd}";
            string bucketName = "Epoch Bucket";
            _ = CreateEpochEventBucket( bucketKey, bucketName );
        }

        /// <summary> Creates Iso8601 event bucket. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        /// <param name="bucketKey">  The bucket key. </param>
        /// <param name="bucketName"> Name of the bucket. </param>
        /// <returns> The new ISO 8601 event bucket. </returns>
        public static Iso8601EventBucket CreateIso8601EventBucket( string bucketKey, string bucketName )
        {
            try
            {
                var eventClient = CreateIso8601EventBucketClient();
                var statusKeyTuple = eventClient.CreateBucket( bucketKey, bucketName );
                Assert.IsTrue( BucketBase.ExpectedCreateBucketStatusCodes().Contains( statusKeyTuple.Item1 ), $"{statusKeyTuple.Item1} is not expected" );
                Assert.AreEqual( bucketKey, statusKeyTuple.Item2, "bucket key should be set correctly" );
                return eventClient;
            }
            catch ( RestApiException ex )
            {
                throw new Exception( $"{ex.Message}. Details: {ex.ResponseContent}" );
            }
        }

        /// <summary> (Unit Test Method) tests create Iso8601 event bucket. </summary>
        /// <remarks> Last tested: 2005-11-19 16:10. </remarks>
        [TestMethod]
        public void CreateIso8601EventBucketTest()
        {
            string bucketKey = $"isrIso8601{DateTimeOffset.Now.Date:yyyyMMdd}";
            string bucketName = "Iso8601 Bucket";
            _ = CreateIso8601EventBucket( bucketKey, bucketName );
        }

        #endregion

        #region " EPOCH EVENT BUCKET TESTS "

        /// <summary> Stream epoch event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        /// <param name="timestamp"> The timestamp. </param>
        public static void StreamEpochEvent( DateTimeOffset timestamp )
        {
            try
            {
                string bucketKey = $"isrTelemetryEpoch{DateTimeOffset.Now.Date:yyyyMMdd}";
                string bucketName = "telemetry";
                using var eventClient = CreateEpochEventBucket( bucketKey, bucketName );
                var rnd = new Random( DateTimeOffset.Now.Second );

                // create a telemetry event
                var telemetry = new Telemetry() {
                    Heading = 1 + ( int ) (360d * rnd.NextDouble()),
                    Speed = 30d + 10d * rnd.NextDouble()
                };
                // Send the event(s)
                eventClient.StreamEvents( telemetry, timestamp, bucketKey, false );
                Assert.AreEqual( My.Settings.Default.ExpectedRateLimit, eventClient.RateLimit.Limit, "expects a specific rate limit" );
                Assert.AreEqual( System.Net.HttpStatusCode.NoContent, eventClient.RateLimit.Status, "expects a specific status code" );
            }
            catch ( RestApiException ex )
            {
                throw new Exception( $"{ex.Message}. Details: {ex.ResponseContent}" );
            }
        }

        /// <summary> (Unit Test Method) tests stream epoch event. </summary>
        /// <remarks> Last tested: 2005-11-19 16:17. </remarks>
        [TestMethod]
        public void StreamEpochEventTest()
        {
            StreamEpochEvent( DateTimeOffset.Now );
        }

        /// <summary> Stream buffered epoch events. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        /// <param name="totalCount"> Number of totals. </param>
        /// <param name="bufferSize"> Size of the buffer. </param>
        /// <param name="timeDelay">  The time delay. </param>
        public static void StreamBufferedEpochEvents( int totalCount, int bufferSize, TimeSpan timeDelay )
        {
            try
            {
                string bucketKey = $"isrTelemetryEpoch{DateTimeOffset.Now.Date:yyyyMMdd}";
                string bucketName = "telemetry";
                using var eventClient = CreateEpochEventBucket( bucketKey, bucketName );
                var rnd = new Random( DateTimeOffset.Now.Second );
                var elements = new List<Telemetry>();
                var timestamps = new List<DateTimeOffset>();
                for ( int i = 1, loopTo = totalCount; i <= loopTo; i++ )
                {
                    elements.Add( new Telemetry() {
                        Heading = 1 + ( int ) (360d * rnd.NextDouble()),
                        Speed = 30d + 10d * rnd.NextDouble()
                    } );
                    timestamps.Add( DateTimeOffset.Now );
                    _ = Dispatcher.CurrentDispatcher.Wait( timeDelay );
                }

                // Send the event(s) buffered using a buffer size of three elements
                eventClient.StreamEvents( elements, timestamps, bufferSize, bucketKey );
                Assert.AreEqual( My.Settings.Default.ExpectedRateLimit, eventClient.RateLimit.Limit, "expects a specific rate limit" );
                Assert.AreEqual( System.Net.HttpStatusCode.NoContent, eventClient.RateLimit.Status, "expects a specific status code" );
            }
            catch ( RestApiException ex )
            {
                throw new Exception( $"{ex.Message}. Details: {ex.ResponseContent}" );
            }
        }

        /// <summary> (Unit Test Method) tests stream nine epoch event buffers. </summary>
        /// <remarks> Last tested: 2005-11-19 16:37. </remarks>
        [TestMethod]
        public void StreamNineEpochEventBuffersTest()
        {
            StreamBufferedEpochEvents( 10, 3, TimeSpan.FromSeconds( 1.1d ) );
        }

        /// <summary> (Unit Test Method) tests stream fifteen epoch event buffers. </summary>
        /// <remarks> Last tested: 2005-11-19 16:37. </remarks>
        [TestMethod]
        public void StreamFifteenEpochEventBuffersTest()
        {
            StreamBufferedEpochEvents( 10, 5, TimeSpan.FromSeconds( 1.1d ) );
        }

        /// <summary> (Unit Test Method) tests stream twenty on epoch event buffers. </summary>
        /// <remarks> Last tested: 2005-11-19 16:37. </remarks>
        [TestMethod]
        public void StreamTwentyOnEpochEventBuffersTest()
        {
            StreamBufferedEpochEvents( 13, 7, TimeSpan.FromSeconds( 1.1d ) );
        }

        #endregion

        #region " CLIENT TESTS "

        /// <summary> (Unit Test Method) tests request last version. </summary>
        /// <remarks> Last tested: 2005-11-19 16:09. </remarks>
        /// <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        [TestMethod]
        public void RequestLastVersionTest()
        {
            _ = ReadAccessKey();
            try
            {
                var client = CreateClient();
                var actualVersion = client.RequestLastVersion();
                var expectedVersion = new Version( My.Settings.Default.ExpectedVersion );
                Assert.IsTrue( expectedVersion.Equals( actualVersion ), $"expected version {expectedVersion} should equal actual version {actualVersion}" );
            }
            catch ( RestApiException ex )
            {
                throw new Exception( $"{ex.Message}. Details: {ex.ResponseContent}" );
            }
        }

        #endregion

    }
}
