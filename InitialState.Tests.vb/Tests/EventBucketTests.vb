Imports System.Windows.Threading

Imports isr.IoT.InitialState
Imports isr.IoT.InitialState.DispatcherExtensions
Namespace InitialStateTests

    ''' <summary> Enum extensions tests. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 3/14/2018 </para>
    ''' </remarks>
    <TestClass()>
    Public Class EventBucketTests

#Region " CONSTRUCTION and CLEANUP "

        ''' <summary> My class initialize. </summary>
        ''' <remarks>
        ''' Use ClassInitialize to run code before running the first test in the class.
        ''' </remarks>
        ''' <param name="testContext"> Gets or sets the test context which provides information about
        '''                            and functionality for the current test run. </param>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <ClassInitialize()>
        Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
            Try
            Catch
                ' cleanup to meet strong guarantees
                Try
                    MyClassCleanup()
                Finally
                End Try
                Throw
            End Try
        End Sub

        ''' <summary> My class cleanup. </summary>
        ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        <ClassCleanup()>
        Public Shared Sub MyClassCleanup()
            EventBucketTests.AssignBucketBase(Nothing)
        End Sub

        ''' <summary> Initializes before each test runs. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        <TestInitialize()> Public Sub MyTestInitialize()
        End Sub

        ''' <summary> Cleans up after each test has run. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        <TestCleanup()> Public Sub MyTestCleanup()
        End Sub

        ''' <summary>
        ''' Gets the test context which provides information about and functionality for the current test
        ''' run.
        ''' </summary>
        ''' <value> The test context. </value>
        Public Property TestContext() As TestContext

#End Region

#Region " EVENT ELEMENT CLASSES "

        ''' <summary> A telemetry. </summary>
        ''' <remarks>
        ''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        ''' Licensed under The MIT License.</para><para>
        ''' David, 5/4/2019 </para>
        ''' </remarks>
        Friend Class Telemetry

            ''' <summary> Gets the heading. </summary>
            ''' <value> The heading. </value>
            Public Property Heading As Integer

            ''' <summary> Gets the speed. </summary>
            ''' <value> The speed. </value>
            Public Property Speed As Double

            ''' <summary> Gets the status. </summary>
            ''' <value> The status. </value>
            Public ReadOnly Property Status As String
                Get
                    Return If(Heading > 270, "Q4", If(Heading > 180, "Q3", If(Heading > 90, "Q2", "Q1")))
                End Get
            End Property
        End Class

#End Region

#Region " CLIENT "

        ''' <summary> Reads access key. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <returns> The access key. </returns>
        Private Shared Function ReadAccessKey() As String
            Return My.Computer.FileSystem.ReadAllText(My.Settings.AccessKeyFileName)
        End Function

        ''' <summary> Handles the exception event. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Error event information. </param>
        Private Shared Sub HandleExceptionEvent(ByVal sender As Object, ByVal e As IO.ErrorEventArgs)
            Assert.IsNotNull(e, $"{e.GetException.ToString}")
        End Sub

        ''' <summary> Gets or sets the bucket base. </summary>
        ''' <value> The bucket base. </value>
        Private Shared ReadOnly Property BucketBase As BucketBase

        ''' <summary> Assign bucket base. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <param name="value"> The value. </param>
        Private Shared Sub AssignBucketBase(ByVal value As BucketBase)
            If EventBucketTests.BucketBase IsNot Nothing Then
                RemoveHandler EventBucketTests.BucketBase.ExceptionOccurred, AddressOf EventBucketTests.HandleExceptionEvent
                EventBucketTests._BucketBase.Dispose()
                EventBucketTests._BucketBase = Nothing
            End If
            EventBucketTests._BucketBase = value
            If EventBucketTests.BucketBase IsNot Nothing Then
                AddHandler EventBucketTests.BucketBase.ExceptionOccurred, AddressOf EventBucketTests.HandleExceptionEvent
            End If
        End Sub

        ''' <summary> Creates epoch event bucket client. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <returns> The new epoch event bucket client. </returns>
        Public Shared Function CreateEpochEventBucketClient() As EpochEventBucket
            Dim accessKey As String = EventBucketTests.ReadAccessKey
            Dim eventClient As New EpochEventBucket(accessKey)
            EventBucketTests.AssignBucketBase(eventClient)
            Return eventClient
        End Function

        ''' <summary> Creates ISO 8601 event bucket client. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <returns> The new ISO 8601 event bucket client. </returns>
        Public Shared Function CreateIso8601EventBucketClient() As Iso8601EventBucket
            Dim accessKey As String = EventBucketTests.ReadAccessKey
            Dim eventClient As New Iso8601EventBucket(accessKey)
            EventBucketTests.AssignBucketBase(eventClient)
            Return eventClient
        End Function

        ''' <summary> Creates the client. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <returns> The new client. </returns>
        Public Shared Function CreateClient() As Client
            Dim accessKey As String = EventBucketTests.ReadAccessKey
            Dim client As New Client(accessKey)
            Return client
        End Function

#End Region

#Region " CREATE BUCKET TESTS "

        ''' <summary> Creates epoch event bucket. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ''' <param name="bucketKey">  The bucket key. </param>
        ''' <param name="bucketName"> Name of the bucket. </param>
        ''' <returns> The new epoch event bucket. </returns>
        Public Shared Function CreateEpochEventBucket(ByVal bucketKey As String, ByVal bucketName As String) As EpochEventBucket
            Try
                Dim eventClient As EpochEventBucket = EventBucketTests.CreateEpochEventBucketClient
                Dim statusKeyTuple As Tuple(Of Net.HttpStatusCode, String) = eventClient.CreateBucket(bucketKey, bucketName)
                Assert.IsTrue(BucketBase.ExpectedCreateBucketStatusCodes.Contains(statusKeyTuple.Item1), $"{statusKeyTuple.Item1} is not expected")
                Assert.AreEqual(bucketKey, statusKeyTuple.Item2, "bucket key should be set correctly")
                Return eventClient
            Catch ex As RestApiException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            End Try
        End Function

        ''' <summary> (Unit Test Method) tests create epoch event bucket. </summary>
        ''' <remarks> Last tested: 05-11-19 16:10. </remarks>
        <TestMethod>
        Public Sub CreateEpochEventBucketTest()
            Dim bucketKey As String = $"isrEpoch{DateTimeOffset.Now.Date.ToString("yyyyMMdd")}"
            Dim bucketName As String = "Epoch Bucket"
            EventBucketTests.CreateEpochEventBucket(bucketKey, bucketName)
        End Sub

        ''' <summary> Creates Iso8601 event bucket. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ''' <param name="bucketKey">  The bucket key. </param>
        ''' <param name="bucketName"> Name of the bucket. </param>
        ''' <returns> The new ISO 8601 event bucket. </returns>
        Public Shared Function CreateIso8601EventBucket(ByVal bucketKey As String, ByVal bucketName As String) As Iso8601EventBucket
            Try
                Dim eventClient As Iso8601EventBucket = EventBucketTests.CreateIso8601EventBucketClient
                Dim statusKeyTuple As Tuple(Of Net.HttpStatusCode, String) = eventClient.CreateBucket(bucketKey, bucketName)
                Assert.IsTrue(BucketBase.ExpectedCreateBucketStatusCodes.Contains(statusKeyTuple.Item1), $"{statusKeyTuple.Item1} is not expected")
                Assert.AreEqual(bucketKey, statusKeyTuple.Item2, "bucket key should be set correctly")
                Return eventClient
            Catch ex As RestApiException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            End Try
        End Function

        ''' <summary> (Unit Test Method) tests create Iso8601 event bucket. </summary>
        ''' <remarks> Last tested: 05-11-19 16:10. </remarks>
        <TestMethod>
        Public Sub CreateIso8601EventBucketTest()
            Dim bucketKey As String = $"isrIso8601{DateTimeOffset.Now.Date.ToString("yyyyMMdd")}"
            Dim bucketName As String = "Iso8601 Bucket"
            EventBucketTests.CreateIso8601EventBucket(bucketKey, bucketName)
        End Sub

#End Region

#Region " EPOCH EVENT BUCKET TESTS "

        ''' <summary> Stream epoch event. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ''' <param name="timestamp"> The timestamp. </param>
        Public Shared Sub StreamEpochEvent(ByVal timestamp As DateTimeOffset)
            Try
                Dim bucketKey As String = $"isrTelemetryEpoch{DateTimeOffset.Now.Date.ToString("yyyyMMdd")}"
                Dim bucketName As String = "telemetry"
                Using eventClient As EpochEventBucket = EventBucketTests.CreateEpochEventBucket(bucketKey, bucketName)

                    Dim rnd As New Random(DateTimeOffset.Now.Second)

                    ' create a telemetry event
                    Dim telemetry As New Telemetry() With {.Heading = 1 + CInt(360 * rnd.NextDouble),
                                                           .Speed = 30 + 10 * rnd.NextDouble}
                    ' Send the event(s)
                    eventClient.StreamEvents(Of Telemetry)(telemetry, timestamp, bucketKey, False)
                    Assert.AreEqual(My.Settings.ExpectedRateLimit, eventClient.RateLimit.Limit, "expects a specific rate limit")
                    Assert.AreEqual(Net.HttpStatusCode.NoContent, eventClient.RateLimit.Status, "expects a specific status code")
                End Using

            Catch ex As RestApiException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            End Try

        End Sub

        ''' <summary> (Unit Test Method) tests stream epoch event. </summary>
        ''' <remarks> Last tested: 05-11-19 16:17. </remarks>
        <TestMethod>
        Public Sub StreamEpochEventTest()
            EventBucketTests.StreamEpochEvent(DateTimeOffset.Now)
        End Sub

        ''' <summary> Stream buffered epoch events. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        ''' <param name="totalCount"> Number of totals. </param>
        ''' <param name="bufferSize"> Size of the buffer. </param>
        ''' <param name="timeDelay">  The time delay. </param>
        Public Shared Sub StreamBufferedEpochEvents(ByVal totalCount As Integer, ByVal bufferSize As Integer, ByVal timeDelay As TimeSpan)
            Try
                Dim bucketKey As String = $"isrTelemetryEpoch{DateTimeOffset.Now.Date.ToString("yyyyMMdd")}"
                Dim bucketName As String = "telemetry"
                Using eventClient As EpochEventBucket = EventBucketTests.CreateEpochEventBucket(bucketKey, bucketName)
                    Dim rnd As New Random(DateTimeOffset.Now.Second)

                    Dim elements As New List(Of Telemetry)
                    Dim timestamps As New List(Of DateTimeOffset)
                    For i As Integer = 1 To totalCount
                        elements.Add(New Telemetry() With {.Heading = 1 + CInt(360 * rnd.NextDouble),
                                                       .Speed = 30 + 10 * rnd.NextDouble})
                        timestamps.Add(DateTimeOffset.Now)
                        Dispatcher.CurrentDispatcher.Wait(timeDelay)
                    Next

                    ' Send the event(s) buffered using a buffer size of three elements
                    eventClient.StreamEvents(Of Telemetry)(elements, timestamps, bufferSize, bucketKey)

                    Assert.AreEqual(My.Settings.ExpectedRateLimit, eventClient.RateLimit.Limit, "expects a specific rate limit")
                    Assert.AreEqual(Net.HttpStatusCode.NoContent, eventClient.RateLimit.Status, "expects a specific status code")

                End Using

            Catch ex As RestApiException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            End Try

        End Sub

        ''' <summary> (Unit Test Method) tests stream nine epoch event buffers. </summary>
        ''' <remarks> Last tested: 05-11-19 16:37. </remarks>
        <TestMethod>
        Public Sub StreamNineEpochEventBuffersTest()
            EventBucketTests.StreamBufferedEpochEvents(10, 3, TimeSpan.FromSeconds(1.1))
        End Sub

        ''' <summary> (Unit Test Method) tests stream fifteen epoch event buffers. </summary>
        ''' <remarks> Last tested: 05-11-19 16:37. </remarks>
        <TestMethod>
        Public Sub StreamFifteenEpochEventBuffersTest()
            EventBucketTests.StreamBufferedEpochEvents(10, 5, TimeSpan.FromSeconds(1.1))
        End Sub

        ''' <summary> (Unit Test Method) tests stream twenty on epoch event buffers. </summary>
        ''' <remarks> Last tested: 05-11-19 16:37. </remarks>
        <TestMethod>
        Public Sub StreamTwentyOnEpochEventBuffersTest()
            EventBucketTests.StreamBufferedEpochEvents(13, 7, TimeSpan.FromSeconds(1.1))
        End Sub

#End Region

#Region " CLIENT TESTS "

        ''' <summary> (Unit Test Method) tests request last version. </summary>
        ''' <remarks> Last tested: 05-11-19 16:09. </remarks>
        ''' <exception cref="Exception"> Thrown when an exception error condition occurs. </exception>
        <TestMethod>
        Public Sub RequestLastVersionTest()
            Dim accessKey As String = EventBucketTests.ReadAccessKey
            Try
                Dim client As Client = EventBucketTests.CreateClient

                Dim actualVersion As Version = client.RequestLastVersion()
                Dim expectedVersion As Version = New Version(My.Settings.ExpectedVersion)
                Assert.IsTrue(expectedVersion.Equals(actualVersion), $"expected version {expectedVersion} should equal actual version {actualVersion}")

            Catch ex As RestApiException
                Throw New Exception($"{ex.Message}. Details: {ex.ResponseContent}")
            End Try

        End Sub

#End Region

    End Class
End Namespace
