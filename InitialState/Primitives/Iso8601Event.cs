
namespace isr.IoT.InitialState
{

    /// <summary> An event using ISRO 8610 timestamp. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-05-04 </para>
    /// </remarks>
    internal class Iso8601Event
    {

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> Gets or sets the key. </summary>
        /// <value> The key. </value>
        public string key { get; set; }
        #pragma warning  restore IDE1006 // Naming Styles

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public string value { get; set; }
        #pragma warning  restore IDE1006 // Naming Styles

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> Gets or sets the timestamp in ISO 8601 format. </summary>
        /// <value> The ISO 8601. </value>
        public string iso8601 { get; set; }
        #pragma warning  restore IDE1006 // Naming Styles

    }
}
