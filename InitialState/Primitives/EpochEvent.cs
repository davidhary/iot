
namespace isr.IoT.InitialState
{

    /// <summary> An event using Unit Epoch time as a timestamp. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-05-04 </para>
    /// </remarks>
    internal class EpochEvent
    {

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> Gets or sets the key. </summary>
        /// <value> The key. </value>
        public string key { get; set; }
        #pragma warning  restore IDE1006 // Naming Styles

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> Gets or sets the value. </summary>
        /// <value> The value. </value>
        public string value { get; set; }
        #pragma warning  restore IDE1006 // Naming Styles

        #pragma warning disable IDE1006 // Naming Styles

        /// <summary> Gets or sets the epoch. </summary>
        /// <value> The epoch. </value>
        public double epoch { get; set; }
        #pragma warning  restore IDE1006 // Naming Styles

    }
}
