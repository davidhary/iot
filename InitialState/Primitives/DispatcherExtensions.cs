using System;
using System.Diagnostics;
using System.Windows.Threading;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.IoT.InitialState.DispatcherExtensions
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Dispatcher extension methods. </summary>
        /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary> Executes the events operation. </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/4502037/where-is-the-application-doevents-in-wpf. Well, I
        /// just hit a case where I start work on a method that runs on the Dispatcher thread, and it
        /// needs to block without blocking the UI Thread. Turns out that MSDN topic on the Dispatcher
        /// Push Frame method explains how to implement a DoEvents() based on the Dispatcher itself:
        /// https://goo.gl/WGFZEU
        /// <code>
        /// Imports System.Windows.Threading
        /// Imports DispatcherExtensions
        /// Dispatcher.CurrentDispatcher.DoEvents
        /// </code>
        /// <list type="bullet">Benchmarks<item>
        /// 1000 iterations of Application Do Events:   7.8,  8.0, 11,  8.4 us </item><item>
        /// 1000 iterations of Dispatcher Do Events:   57.6, 92.5, 93, 61.6 us </item></list>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dispatcher"> The dispatcher. </param>
        public static void DoEvents( this Dispatcher dispatcher )
        {
            if ( dispatcher is null )
                throw new ArgumentNullException( nameof( dispatcher ) );
            var frame = new DispatcherFrame();
            _ = dispatcher.BeginInvoke( DispatcherPriority.Background, new DispatcherOperationCallback( ExitFrame ), frame );
            Dispatcher.PushFrame( frame );
        }

        /// <summary> Exit frame. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="f"> references to the <see cref="DispatcherFrame"/>. </param>
        /// <returns> An Object. </returns>
        private static object ExitFrame( object f )
        {
            (f as DispatcherFrame).Continue = false;
            return null;
        }

        /// <summary>
        /// Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dispatcher"> The dispatcher. </param>
        /// <param name="act">        The act. </param>
        public static void Render( this Dispatcher dispatcher, Action act )
        {
            if ( dispatcher is null )
                throw new ArgumentNullException( nameof( dispatcher ) );
            _ = dispatcher.Invoke( DispatcherPriority.Render, act );
        }

        /// <summary> Waits for the specified time to elapse. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dispatcher"> The dispatcher. </param>
        /// <param name="stopwatch">  The stopwatch. </param>
        /// <param name="value">      The value. </param>
        /// <returns> The elapsed time span including any time already elapsed on the stopwatch. </returns>
        public static TimeSpan Wait( this Dispatcher dispatcher, Stopwatch stopwatch, TimeSpan value )
        {
            if ( dispatcher is null )
                throw new ArgumentNullException( nameof( dispatcher ) );
            if ( stopwatch is object && value > TimeSpan.Zero )
            {
                if ( !stopwatch.IsRunning )
                    stopwatch.Restart();
                value = value.Add( stopwatch.Elapsed );
                do
                    dispatcher.DoEvents();
                while ( stopwatch.Elapsed < value );
                return stopwatch.Elapsed;
            }
            else
            {
                return TimeSpan.Zero;
            }
        }

        /// <summary> Waits for the specified time to elapse. </summary>
        /// <remarks>
        /// <list type="bullet">Benchmarks:<item>
        /// 1000 iterations of 1ms:   1.10, 1.06ms </item><item>
        /// 100 iterations of 10ms:  10.1, 10.07ms </item><item>
        /// 10 iterations of 100ms: 100.05, 100.15ms </item></list>
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dispatcher"> The dispatcher. </param>
        /// <param name="value">      The value. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan Wait( this Dispatcher dispatcher, TimeSpan value )
        {
            if ( dispatcher is null )
                throw new ArgumentNullException( nameof( dispatcher ) );
            var stopwatch = Stopwatch.StartNew();
            do
                dispatcher.DoEvents();
            while ( stopwatch.Elapsed < value );
            return stopwatch.Elapsed;
        }

        /// <summary> Waits while the stop watch is running until its elapsed time expires. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="dispatcher"> The dispatcher. </param>
        /// <param name="stopwatch">  The stop watch. </param>
        /// <param name="value">      The value. </param>
        /// <returns> The elapsed <see cref="System.TimeSpan"/> </returns>
        public static TimeSpan LetElapse( this Dispatcher dispatcher, Stopwatch stopwatch, TimeSpan value )
        {
            if ( dispatcher is null )
                throw new ArgumentNullException( nameof( dispatcher ) );
            if ( stopwatch is object && value > TimeSpan.Zero && stopwatch.IsRunning )
            {
                do
                    dispatcher.DoEvents();
                while ( stopwatch.Elapsed <= value );
                return stopwatch.Elapsed;
            }
            else
            {
                return TimeSpan.Zero;
            }
        }
    }
}
