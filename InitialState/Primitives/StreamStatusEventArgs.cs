﻿using System;

using RestSharp;

namespace isr.IoT.InitialState
{

    /// <summary> Additional information for Stream status events. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-09 </para>
        /// </remarks>
    public class StreamStatusEventArgs : EventArgs
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="status"> The status. </param>
        public StreamStatusEventArgs( System.Net.HttpStatusCode status ) : base()
        {
            this.Status = status;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="response"> The Stream. </param>
        public StreamStatusEventArgs( RestResponse response ) : base()
        {
            this.Status = response is null ? System.Net.HttpStatusCode.OK : response.StatusCode;
        }

        /// <summary> Gets the okay. </summary>
        /// <value> The okay. </value>
        public static StreamStatusEventArgs Okay => new StreamStatusEventArgs( System.Net.HttpStatusCode.OK );

        /// <summary> Gets or sets the status. </summary>
        /// <value> The status. </value>
        public System.Net.HttpStatusCode Status { get; set; }
    }
}