﻿using System;
using System.Linq;
using System.Net;

namespace isr.IoT.InitialState
{

    /// <summary> Exception throw if either API or <see cref="RestSharp"/> errors occurred. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-04 </para>
        /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable", Justification = "<Pending>" )]
    public class RestApiException : Exception
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public RestApiException() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="message"> The message. </param>
        public RestApiException( string message ) : base( message )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> The inner exception. </param>
        public RestApiException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="message">         The message. </param>
        /// <param name="statusCode">      The status code. </param>
        /// <param name="firstParameter">  The first parameter of the
        /// <see cref="RestSharp.IRestRequest">request</see> body. </param>
        /// <param name="responseContent"> The <see cref="RestSharp.IRestResponse">response</see>
        /// content. </param>
        public RestApiException( string message, HttpStatusCode statusCode, string firstParameter, string responseContent ) : base( message )
        {
            this.StatusCode = statusCode;
            this.RequestFirstParameter = firstParameter;
            this.ResponseContent = responseContent;
        }

        /// <summary> Validated response. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="response"> The <see cref="RestSharp.IRestResponse">response</see>. </param>
        /// <returns> A RestSharp.IRestResponse. </returns>
        private static RestSharp.IRestResponse ValidatedResponse( RestSharp.IRestResponse response )
        {
            return response is null ? throw new ArgumentNullException( nameof( response ) ) : response;
        }

        /// <summary>
        /// Extracts the first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="request"> The <see cref="RestSharp.IRestRequest">request</see>. </param>
        /// <returns>
        /// The extracted first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
        /// </returns>
        private static string ExtractRequestBodyFirstParameter( RestSharp.IRestRequest request )
        {
            if ( request is null )
                throw new ArgumentNullException( nameof( request ) );
            string result = "<empty>";
            var firstParameter = request.Parameters.FirstOrDefault( x => x.Type == RestSharp.ParameterType.RequestBody );
            if ( firstParameter is object )
                result = firstParameter.ToString();
            return result;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="message">  The message. </param>
        /// <param name="response"> The <see cref="RestSharp.IRestResponse">response</see>. </param>
        /// <param name="request">  The <see cref="RestSharp.IRestRequest">request</see>. </param>
        public RestApiException( string message, RestSharp.IRestResponse response, RestSharp.IRestRequest request ) : this( message, ValidatedResponse( response ).ErrorException, response.StatusCode, ExtractRequestBodyFirstParameter( request ), response.Content )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="statusCode">      The status code. </param>
        /// <param name="firstParameter">  The first parameter of the
        /// <see cref="RestSharp.IRestRequest">request</see> body. </param>
        /// <param name="responseContent"> The <see cref="RestSharp.IRestResponse">response</see>
        /// content. </param>
        public RestApiException( HttpStatusCode statusCode, string firstParameter, string responseContent ) : base()
        {
            this.StatusCode = statusCode;
            this.RequestFirstParameter = firstParameter;
            this.ResponseContent = responseContent;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="message">         The message. </param>
        /// <param name="exception">       The exception. </param>
        /// <param name="statusCode">      The status code. </param>
        /// <param name="firstParameter">  The first parameter of the
        /// <see cref="RestSharp.IRestRequest">request</see> body. </param>
        /// <param name="responseContent"> The <see cref="RestSharp.IRestResponse">response</see>
        /// content. </param>
        public RestApiException( string message, Exception exception, HttpStatusCode statusCode, string firstParameter, string responseContent ) : base( message, exception )
        {
            this.StatusCode = statusCode;
            this.RequestFirstParameter = firstParameter;
            this.ResponseContent = responseContent;
        }

        /// <summary> Gets or sets the status code. </summary>
        /// <value> The status code. </value>
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>
        /// Gets or sets the first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
        /// </summary>
        /// <value>
        /// The first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
        /// </value>
        public string RequestFirstParameter { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="RestSharp.IRestResponse">response</see> content.
        /// </summary>
        /// <value> The <see cref="RestSharp.IRestResponse">response</see> content. </value>
        public string ResponseContent { get; set; }
    }
}