﻿using System;
using System.Threading.Tasks;

namespace isr.IoT.InitialState
{

    /// <summary> Additional information for task status events. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-09 </para>
        /// </remarks>
    public class TaskStatusEventArgs : EventArgs
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="status"> The status. </param>
        public TaskStatusEventArgs( TaskStatus status ) : base()
        {
            this.Status = status;
        }

        /// <summary> Gets the event arguments for the created status. </summary>
        /// <value> The event arguments for the created status. </value>
        public static TaskStatusEventArgs Created => new TaskStatusEventArgs( TaskStatus.Created );

        /// <summary> Gets or sets the status. </summary>
        /// <value> The status. </value>
        public TaskStatus Status { get; set; }
    }
}