﻿using System;
using System.Collections.Generic;

namespace isr.IoT.InitialState.StringExtensions
{

    /// <summary> Includes 'include' extensions for <see cref="String">String</see>. </summary>
        /// <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2009-04-09, 1.1.3386.x. </para></remarks>
    public static class Methods
    {

        /// <summary> Returns true if the string includes the specified characters. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">     The string that is being searched. </param>
        /// <param name="characters"> The characters to search for. </param>
        /// <returns>
        /// <c>True</c> if the string includes the specified characters; otherwise, <c>False</c>.
        /// </returns>
        public static bool IncludesCharacters( this string source, string characters )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( characters is null )
                throw new ArgumentNullException( nameof( characters ) );
            string expression = $"[{characters}]";
            var r = new System.Text.RegularExpressions.Regex( expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase );
            return r.IsMatch( source );
        }

        /// <summary> Enumerates matching characters in this collection. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">     The string that is being searched. </param>
        /// <param name="characters"> The characters to search for. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process matching characters in this
        /// collection.
        /// </returns>
        public static IEnumerable<string> MatchingCharacters( this string source, string characters )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( characters is null )
                throw new ArgumentNullException( nameof( characters ) );
            string expression = $"[{characters}]";
            var r = new System.Text.RegularExpressions.Regex( expression, System.Text.RegularExpressions.RegexOptions.IgnoreCase );
            var matches = r.Matches( source );
            if ( matches.Count == 0 )
            {
                return Array.Empty<string>();
            }
            else
            {
                var l = new List<string>();
                foreach ( System.Text.RegularExpressions.Match m in matches )
                    l.Add( m.Value );
                return l;
            }
        }
    }
}