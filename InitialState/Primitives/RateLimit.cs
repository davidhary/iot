using System;
using System.Collections.Generic;
using System.Linq;

using RestSharp;

namespace isr.IoT.InitialState
{

    /// <summary> A rate limit. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-08 </para>
        /// </remarks>
    public class RateLimit
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public RateLimit() : base()
        {
            this.Limit = AccountPlanRateLimit * RateLimitSamplePeriod;
            this.Remaining = this.Limit;
            this.Reset = RateLimitSamplePeriod;
            this.IsHitRateLimit = false;
            this.RetryAfterTimespan = TimeSpan.Zero;
            this.Status = System.Net.HttpStatusCode.OK;
        }

        /// <summary> Gets the account plan rate limit. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The account plan rate limit. </value>
        public static int AccountPlanRateLimit { get; set; } = 3;

        /// <summary> Gets the rate limit sample period in seconds. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The rate limit sample period. </value>
        public static int RateLimitSamplePeriod { get; set; } = 10;

        /// <summary> Parses the given response. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="response"> The response. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails" )]
        public void Parse( IRestResponse response )
        {
            if ( response is null )
                throw new ArgumentNullException( nameof( response ) );
            List<Parameter> l = null;
            try
            {
                l = response.Headers.ToList();
                this.Status = response.StatusCode;
                string headerValue = string.Empty;
                headerValue = l.Find( x => (x.Name ?? "") == _LimitHeader )?.Value.ToString();
                if ( !string.IsNullOrEmpty( headerValue ) )
                    this.Limit = int.Parse( headerValue );
                headerValue = l.Find( x => (x.Name ?? "") == _RemainingHeader )?.Value.ToString();
                if ( !string.IsNullOrEmpty( headerValue ) )
                    this.Remaining = int.Parse( headerValue );
                headerValue = l.Find( x => (x.Name ?? "") == _ResetHeader )?.Value.ToString();
                if ( !string.IsNullOrEmpty( headerValue ) )
                    this.Reset = long.Parse( headerValue );
                this.IsHitRateLimit = ( int ) response.StatusCode == RateLimitedRequestStatusCode;
                if ( this.IsHitRateLimit )
                {
                    headerValue = l.Find( x => (x.Name ?? "") == _RetryAfterHeader )?.Value.ToString();
                    if ( !string.IsNullOrEmpty( headerValue ) )
                        this.UpdateRetry( int.Parse( headerValue ) );
                    this.SuccessiveRateLimitCountOut -= 1;
                }
                else
                {
                    this.RetryAfterTimespan = TimeSpan.Zero;
                    this.SuccessiveRateLimitCountOut = DefaultSuccessiveRateLimitCountOut;
                }
            }
            catch ( Exception ex )
            {
                if ( l is object )
                {
                    foreach ( Parameter p in l )
                        ex.Data.Add( $"{p.Name}=", p.Value );
                }

                throw ex;
            }
        }

        /// <summary> The limit header. </summary>
        private const string _LimitHeader = "X-RateLimit-Limit";

        /// <summary> The remaining header. </summary>
        private const string _RemainingHeader = "X-RateLimit-Remaining";

        /// <summary> The reset header. </summary>
        private const string _ResetHeader = "X-RateLimit-Reset";

        /// <summary> The retry after header. </summary>
        private const string _RetryAfterHeader = "Retry-After";

        /// <summary> The rate limited request status code. </summary>
        public const int RateLimitedRequestStatusCode = 429; // too many requests

        /// <summary> Gets the status. </summary>
        /// <value> The status. </value>
        public System.Net.HttpStatusCode Status { get; set; }

        /// <summary> Gets the rate limit in requests per 10 second interval. </summary>
        /// <remarks>
        /// API Header: <c>X-RateLimit-Limit</c><para>
        /// This Is the current number of requests per 10 second interval. This value is based on the
        /// account plan and accessKey rights</para>
        /// </remarks>
        /// <value> The rate limit in requests per 10 second interval. </value>
        public int Limit { get; set; }

        /// <summary>
        /// Gets the number of requests left from the <see cref="Limit"/> during a 10 second interval.
        /// </summary>
        /// <remarks>
        /// API Header: <c>X-RateLimit-Remaining</c><para>
        /// The number of requests left from the <see cref="Limit"/> during a 10 second interval</para>
        /// </remarks>
        /// <value>
        /// The number of requests left from the <see cref="Limit"/> during a 10 second interval.
        /// </value>
        public int Remaining { get; set; }

        /// <summary>
        /// Gets the Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back
        /// to the value of <see cref="Limit"/>.
        /// </summary>
        /// <remarks>
        /// API Header: <c>X-RateLimit-Reset</c><para>
        /// The Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back to
        /// the value of <see cref="Limit"/></para>
        /// </remarks>
        /// <value>
        /// The Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back to
        /// the value of <see cref="Limit"/>.
        /// </value>
        public long Reset { get; set; }

        /// <summary>
        /// Gets time to wait until the <see cref="Remaining"/> will be reset back to the value of
        /// <see cref="Limit"/>.
        /// </summary>
        /// <value> The reset timespan. </value>
        public TimeSpan ResetTimespan => this.ResetTime.Subtract( DateTimeOffset.Now );

        /// <summary> Gets the <see cref="Reset"/> UTC time. </summary>
        /// <value> The <see cref="Reset"/> UTC time. </value>
        public DateTimeOffset ResetTime => BucketBase.EpochStartTime.AddSeconds( this.Reset );

        /// <summary> Returns <c>True</c> if the response failed hitting the rate limit. </summary>
        /// <value>
        /// <c>True</c> if the response failed hitting the rate limit; <c>False</c> otherwise.
        /// </value>
        public bool IsHitRateLimit { get; private set; }

        /// <summary>
        /// Updates the number of seconds requesting code should wait before retrying the request.
        /// </summary>
        /// <remarks>
        /// API Header: <c>Retry-After</c><para>
        /// This number of seconds requesting code should wait before retrying the request</para><para>
        /// This header Is only returned during a rate limited request represented by an HTTP 429 Status
        /// Code.
        /// </para>
        /// </remarks>
        /// <param name="retryDelay"> The retry delay. This number of seconds requesting code should wait
        /// before retrying the request. </param>
        public void UpdateRetry( long retryDelay )
        {
            this.RetryAfterTimespan = retryDelay > 0L ? TimeSpan.FromSeconds( retryDelay ) : TimeSpan.Zero;
        }

        /// <summary> Gets the retry after timespan relative to the current time. </summary>
        /// <value> The retry after timespan relative to the current time. </value>
        public TimeSpan RetryAfterTimespan { get; private set; }

        /// <summary> Gets the retry after UTC time. </summary>
        /// <value> The retry after UTC time. </value>
        public DateTimeOffset RetryAfterTime => DateTimeOffset.Now.Add( this.RetryAfterTimespan );

        /// <summary>
        /// Gets an indications if a hold is required, each as due to hitting rate limit or reaching the
        /// request count <see cref="Limit"/>
        /// </summary>
        /// <value> The indication that a pause is required. </value>
        public bool IsPauseRequired => this.IsHitRateLimit || this.Remaining <= 0;

        /// <summary>
        /// Gets the time to wait after <see cref="Reset"/> or <see cref="IsHitRateLimit"/> before
        /// streaming new data.
        /// </summary>
        /// <value> The pause timespan. </value>
        public TimeSpan PauseTimespan => this.IsHitRateLimit ? this.RetryAfterTimespan : this.Remaining <= 0 ? this.ResetTimespan : TimeSpan.Zero;

        /// <summary>
        /// Gets or sets the rate limit count out. This value restarts on each successful stream. The
        /// value decrements each time a rate limit is hit. It is used to raise an exception after a
        /// stream of successive rate limits.
        /// </summary>
        /// <value> The rate limit count out. </value>
        public int SuccessiveRateLimitCountOut { get; private set; }

        /// <summary> Gets or sets the default successive rate limit count out. </summary>
        /// <value> The default successive rate limit count out. </value>
        public static int DefaultSuccessiveRateLimitCountOut { get; set; } = 10;
    }
}
