using System;

using RestSharp;
using RestSharp.Serialization.Json;

namespace isr.IoT.InitialState
{

    /// <summary> An initial state bucket base class. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-10 </para>
        /// </remarks>
    public abstract partial class BucketBase : Client, IDisposable
    {

        #region " CONSTRUCTION AND CLEANUP "

        /// <summary>
        /// Creates an API Client for creating Initial State Buckets and sending events to the bucket.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="accessKey"> The access key. </param>
        protected BucketBase( string accessKey ) : base( accessKey )
        {
            this.RateLimit = new RateLimit();
        }

        /// <summary> Gets or sets the is disposed. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        this.TaskCancellationSource?.Cancel();
                        if ( this.ActionTask is object )
                        {
                            this.ActionTask.Dispose();
                            this.ActionTask = null;
                        }

                        if ( this.TaskCancellationSource is object )
                        {
                            this.TaskCancellationSource.Dispose();
                            this.TaskCancellationSource = null;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            // uncomment the following line if Finalize() is overridden above.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        ~BucketBase()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " BUCKET "

        /// <summary> The illegal bucket key characters. </summary>
        public const string IllegalBucketKeyCharacters = @"/\\:*?""<>|.";

        /// <summary> Creates a bucket key using GUID. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The new bucket key. </returns>
        public static string CreateBucketKey()
        {
            return Guid.NewGuid().ToString( "N" );
        }

        /// <summary> Access bucket. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="bucketKey"> the bucket key associated with this instantiation. </param>
        /// <returns> A String. </returns>
        protected string AccessBucket( string bucketKey )
        {
            return $"{this.AccessKey.Substring( 0, 4 )}...{this.AccessKey.Substring( this.AccessKey.Length - 5, 4 )}:{bucketKey}";
        }

        /// <summary>   Create a bucket to receive events. </summary>
        /// <remarks>   David, 2020-10-27. </remarks>
        /// <param name="name"> The name of the bucket. </param>
        /// <returns>   The new bucket. </returns>
        public Tuple<System.Net.HttpStatusCode, string> CreateBucket( string name )
        {
            return this.CreateBucket( CreateBucketKey(), name, null );
        }

        /// <summary>   Create a bucket to receive events. </summary>
        /// <remarks>   David, 2020-10-27. </remarks>
        /// <param name="key">  The unique key of the bucket in the scope of the Access Key. </param>
        /// <param name="name"> The name of the bucket. </param>
        /// <returns>   The new bucket. </returns>
        public Tuple<System.Net.HttpStatusCode, string> CreateBucket( string key, string name )
        {
            return this.CreateBucket( key, name, null );
        }

        /// <summary>   Create a bucket to receive events. </summary>
        /// <remarks>   David, 2020-10-27. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <exception cref="RestApiException">             Thrown if either API or
        ///                                                 <see cref="RestSharp"/>
        ///                                                 errors occurred. </exception>
        /// <param name="key">  The unique key of the bucket in the scope of the Access Key. </param>
        /// <param name="name"> The name of the bucket. </param>
        /// <param name="tags"> An array of string tags to attach to the bucket upon creation; could be
        ///                     empty. </param>
        /// <returns>   The new bucket. </returns>
        public Tuple<System.Net.HttpStatusCode, string> CreateBucket( string key, string name, string[] tags )
        {
            if ( StringExtensions.Methods.IncludesCharacters( key, IllegalBucketKeyCharacters ) )
            {
                throw new InvalidOperationException( $"Bucket key {key} must not include any of {IllegalBucketKeyCharacters}" );
            }

            var request = new RestRequest( BucketsEndPoint, Method.POST );
            _ = request.AddHeader( AccessKeyHeader, this.AccessKey );
            _ = request.AddHeader( ContentTypeHeader, JsonContentType );
            _ = request.AddHeader( AcceptVersionHeader, ApiVersion );
            request.JsonSerializer = new JsonSerializer();
            _ = tags is null
                ? request.AddJsonBody( new { bucketName = name, bucketKey = key } )
                : request.AddJsonBody( new { bucketName = name, bucketKey = key, tags } );

            var response = this.RestClient.Execute( request );
            return Client.IsFailed( response.StatusCode, ExpectedCreateBucketStatusCodes() )
                ? throw new RestApiException( $"{response.StatusCode} creating bucket {response.ResponseUri}/{this.AccessBucket( key )}", response, request )
                : new Tuple<System.Net.HttpStatusCode, string>( response.StatusCode, key );
        }

        /// <summary> Gets the expected status codes for creating a bucket. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The expected status codes for creating a bucket. </returns>
        public static System.Net.HttpStatusCode[] ExpectedCreateBucketStatusCodes()
        {
            return new System.Net.HttpStatusCode[] { System.Net.HttpStatusCode.OK, System.Net.HttpStatusCode.Created, System.Net.HttpStatusCode.NoContent };
        }

        #endregion

        #region " EXCEPTION HANDLERS "

        /// <summary> The exception occurred. </summary>
        public event EventHandler<System.IO.ErrorEventArgs> ExceptionOccurred;

        /// <summary> Raises the error event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnExceptionOccurred( System.IO.ErrorEventArgs e )
        {
            var evt = ExceptionOccurred;
            evt?.Invoke( this, e );
        }

        /// <summary> Raises the error event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="exception"> The exception. </param>
        protected void OnExceptionOccurred( Exception exception )
        {
            this.OnExceptionOccurred( new System.IO.ErrorEventArgs( exception ) );
        }

        #endregion

    }
}
