﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using RestSharp;
using RestSharp.Serialization.Json;

namespace isr.IoT.InitialState
{

    /// <summary> An epoch event bucket. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-10 </para>
        /// </remarks>
    public class EpochEventBucket : BucketBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="accessKey"> The access key. </param>
        public EpochEventBucket( string accessKey ) : base( accessKey )
        {
        }

        /// <summary>
        /// Enumerates the events to stream to the bucket as <see cref="EpochEvent">events</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="entity">    The entity which properties are streamed to the bucket as
        /// <see cref="EpochEvent">events</see>. </param>
        /// <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the events in this collection.
        /// </returns>
        private IEnumerable<EpochEvent> EnumerateEvents<T>( T entity, DateTime timestamp )
        {
            double epoch = ToUnixEpochTime( timestamp );
            IEnumerable<EpochEvent> events = null;
            if ( typeof( T ).IsAssignableFrom( typeof( IDictionary<string, string> ) ) )
            {
                events = (( IDictionary<string, string> ) entity).Select( kvp => new EpochEvent() {
                    epoch = epoch,
                    key = kvp.Key,
                    value = kvp.Value
                } );
            }
            else
            {
                var properties = typeof( T ).GetProperties();
                events = properties.Select( prop => new EpochEvent() {
                    epoch = epoch,
                    key = prop.Name,
                    value = prop.GetValue( entity ).ToString()
                } );
            }

            return events;
        }

        /// <summary> Stream events. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="entities">   The entities. </param>
        /// <param name="timestamps"> The timestamps. </param>
        /// <param name="bucketKey">  The bucket key. </param>
        /// <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public override void StreamEvents<T>( IEnumerable<T> entities, IEnumerable<DateTime> timestamps, string bucketKey, bool sendAsync )
        {
            if ( entities is null || !entities.Any() )
                throw new ArgumentNullException( nameof( entities ) );
            if ( timestamps is null || !timestamps.Any() )
                throw new ArgumentNullException( nameof( timestamps ) );
            if ( entities.Count() != timestamps.Count() )
                throw new ArgumentException( nameof( entities ), $"Number of {nameof( entities )} {entities.Count()} must match the number of {nameof( timestamps )} {timestamps.Count()}" );
            var events = new List<EpochEvent>();
            int i = 0;
            foreach ( T obj in entities )
            {
                events.AddRange( this.EnumerateEvents( obj, timestamps.ElementAtOrDefault( i ) ) );
                i += 1;
            }

            this.StreamEvents( events, bucketKey, sendAsync );
        }

        /// <summary> Stream events. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="obj">       The object. </param>
        /// <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
        /// <param name="bucketKey"> The bucket key. </param>
        /// <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public override void StreamEvents<T>( T obj, DateTime timestamp, string bucketKey, bool sendAsync )
        {
            if ( obj is null )
                throw new ArgumentNullException( nameof( obj ) );
            double epoch = ToUnixEpochTime( timestamp );
            IEnumerable<EpochEvent> events = null; // List(Of [Event]) = Nothing
            if ( typeof( T ).IsAssignableFrom( typeof( IDictionary<string, string> ) ) )
            {
                events = (( IDictionary<string, string> ) obj).Select( kvp => new EpochEvent() {
                    epoch = epoch,
                    key = kvp.Key,
                    value = kvp.Value
                } );
            }
            else
            {
                var properties = typeof( T ).GetProperties();
                events = properties.Select( prop => new EpochEvent() {
                    epoch = epoch,
                    key = prop.Name,
                    value = prop.GetValue( obj ).ToString()
                } );
            }

            this.StreamEvents( events, bucketKey, sendAsync );
        }

        /// <summary>
        /// Enumerates the events to stream to the bucket as <see cref="EpochEvent">events</see>.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="entity">    The entity which properties are streamed to the bucket as
        /// <see cref="EpochEvent">events</see>. </param>
        /// <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the events in this collection.
        /// </returns>
        private IEnumerable<EpochEvent> EnumerateEvents<T>( T entity, DateTimeOffset timestamp )
        {
            double epoch = ToUnixEpochTime( timestamp );
            IEnumerable<EpochEvent> events = null;
            if ( typeof( T ).IsAssignableFrom( typeof( IDictionary<string, string> ) ) )
            {
                events = (( IDictionary<string, string> ) entity).Select( kvp => new EpochEvent() {
                    epoch = epoch,
                    key = kvp.Key,
                    value = kvp.Value
                } );
            }
            else
            {
                var properties = typeof( T ).GetProperties();
                events = properties.Select( prop => new EpochEvent() {
                    epoch = epoch,
                    key = prop.Name,
                    value = prop.GetValue( entity ).ToString()
                } );
            }

            return events;
        }

        /// <summary> Stream events. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="entities">   The entities. </param>
        /// <param name="timestamps"> The timestamps. </param>
        /// <param name="bucketKey">  The bucket key. </param>
        /// <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public override void StreamEvents<T>( IEnumerable<T> entities, IEnumerable<DateTimeOffset> timestamps, string bucketKey, bool sendAsync )
        {
            if ( entities is null || !entities.Any() )
                throw new ArgumentNullException( nameof( entities ) );
            if ( timestamps is null || !timestamps.Any() )
                throw new ArgumentNullException( nameof( timestamps ) );
            if ( entities.Count() != timestamps.Count() )
                throw new ArgumentException( nameof( entities ), $"Number of {nameof( entities )} {entities.Count()} must match the number of {nameof( timestamps )} {timestamps.Count()}" );
            var events = new List<EpochEvent>();
            int i = 0;
            foreach ( T obj in entities )
            {
                events.AddRange( this.EnumerateEvents( obj, timestamps.ElementAtOrDefault( i ) ) );
                i += 1;
            }

            this.StreamEvents( events, bucketKey, sendAsync );
        }

        /// <summary> Stream events. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="obj">       The object. </param>
        /// <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
        /// <param name="bucketKey"> The bucket key. </param>
        /// <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public override void StreamEvents<T>( T obj, DateTimeOffset timestamp, string bucketKey, bool sendAsync )
        {
            if ( obj is null )
                throw new ArgumentNullException( nameof( obj ) );
            double epoch = ToUnixEpochTime( timestamp );
            IEnumerable<EpochEvent> events = null; // List(Of [Event]) = Nothing
            if ( typeof( T ).IsAssignableFrom( typeof( IDictionary<string, string> ) ) )
            {
                events = (( IDictionary<string, string> ) obj).Select( kvp => new EpochEvent() {
                    epoch = epoch,
                    key = kvp.Key,
                    value = kvp.Value
                } );
            }
            else
            {
                var properties = typeof( T ).GetProperties();
                events = properties.Select( prop => new EpochEvent() {
                    epoch = epoch,
                    key = prop.Name,
                    value = prop.GetValue( obj ).ToString()
                } );
            }

            this.StreamEvents( events, bucketKey, sendAsync );
        }

        /// <summary> Streams <see cref="EpochEvent">epoch events</see> </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="events">    The events. </param>
        /// <param name="bucketKey"> The bucket key. </param>
        /// <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
        /// response; otherwise, <c>false</c>. </param>
        internal void StreamEvents( IEnumerable<EpochEvent> events, string bucketKey, bool sendAsync )
        {
            if ( events is null || !events.Any() )
                throw new ArgumentNullException( nameof( events ) );
            if ( string.IsNullOrEmpty( bucketKey ) )
                throw new ArgumentException( "a bucket key is required" );
            string activity = string.Empty;
            var request = new RestRequest( EventsEndPoint, Method.POST );
            _ = request.AddHeader( AccessKeyHeader, this.AccessKey );
            _ = request.AddHeader( BucketKeyHeader, bucketKey );
            _ = request.AddHeader( AcceptVersionHeader, ApiVersion );
            request.JsonSerializer = new JsonSerializer();
            _ = request.AddJsonBody( events );
            activity = $"streaming {EventsEndPoint}";
            if ( sendAsync )
            {
                _ = this.RestClient.ExecuteAsync( request, response => this.OnSendEventCompleted( activity, bucketKey, response, request, sendAsync ) );
            }
            else
            {
                var response = this.RestClient.Execute( request );
                this.OnSendEventCompleted( activity, bucketKey, response, request, sendAsync );
            }
        }
    }
}