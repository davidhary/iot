// 
// Bucket Base Stream
// 
using System;
using System.Collections.Generic;

using RestSharp;

namespace isr.IoT.InitialState
{
    public partial class BucketBase
    {

        #region " STREAM EVENTS "

        /// <summary> Gets or sets the rate limit. </summary>
        /// <value> The rate limit. </value>
        public RateLimit RateLimit { get; set; }

        /// <summary> Streams a single event as key, value and timestamp. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="key">       the key to associate with the <paramref name="value"/> of the event. </param>
        /// <param name="value">     value of the event. </param>
        /// <param name="timestamp"> The timestamp to use for this event. </param>
        /// <param name="bucketKey"> The key of the bucket receiving the events. </param>
        /// <param name="sendAsync"> <c>True</c> to stream the event without waiting for confirmed
        /// response; otherwise, <c>false</c>. </param>
        public void StreamEvent( string key, string value, DateTime timestamp, string bucketKey, bool sendAsync )
        {
            this.StreamEvents( new Dictionary<string, string>() { { key, value } }, timestamp, bucketKey, sendAsync );
        }

        /// <summary> Streams a collection of entities using a single API call. </summary>
        /// <remarks>
        /// An <see cref="RestApiException"/> might occur if the stream hit the event limiting rate.
        /// </remarks>
        /// <param name="entities">   The entities. </param>
        /// <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
        /// <param name="bucketKey">  The key of the bucket receiving the events. </param>
        /// <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public abstract void StreamEvents<T>( IEnumerable<T> entities, IEnumerable<DateTime> timestamps, string bucketKey, bool sendAsync );

        /// <summary>
        /// Streams a single entity as a set of events each representing a property name (as a key) and
        /// value.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="entity">    The entity. </param>
        /// <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
        /// <param name="bucketKey"> the key of the bucket receiving the events. </param>
        /// <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public abstract void StreamEvents<T>( T entity, DateTime timestamp, string bucketKey, bool sendAsync );

        /// <summary> Streams a single event as key, value and timestamp. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="key">       the key to associate with the <paramref name="value"/> of the event. </param>
        /// <param name="value">     value of the event. </param>
        /// <param name="timestamp"> The timestamp to use for this event. </param>
        /// <param name="bucketKey"> The key of the bucket receiving the events. </param>
        /// <param name="sendAsync"> <c>True</c> to stream the event without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public void StreamEvent( string key, string value, DateTimeOffset timestamp, string bucketKey, bool sendAsync )
        {
            this.StreamEvents( new Dictionary<string, string>() { { key, value } }, timestamp, bucketKey, sendAsync );
        }

        /// <summary>
        /// Streams a collection of entities using a multiple API calls each streaming a 
        /// fixed buffer size of entities.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="entities">   The entities. </param>
        /// <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
        /// <param name="bucketKey">  The key of the bucket receiving the events. </param>
        /// <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public abstract void StreamEvents<T>( IEnumerable<T> entities, IEnumerable<DateTimeOffset> timestamps, string bucketKey, bool sendAsync );

        /// <summary>
        /// Streams a collection of entities using a multiple API calls each streaming a
        /// fixed buffer size of entities.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="entity">    The entity. </param>
        /// <param name="timestamp"> The timestamp to use for this event. </param>
        /// <param name="bucketKey"> The key of the bucket receiving the events. </param>
        /// <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
        /// response;
        /// otherwise, <c>false</c>. </param>
        public abstract void StreamEvents<T>( T entity, DateTimeOffset timestamp, string bucketKey, bool sendAsync );

        /// <summary> Gets the expected status codes for streaming events. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The expected status codes for streaming events. </returns>
        public static System.Net.HttpStatusCode[] ExpectedStreamEventsStatusCodes()
        {
            return new System.Net.HttpStatusCode[] { System.Net.HttpStatusCode.OK, System.Net.HttpStatusCode.Created, System.Net.HttpStatusCode.NoContent };
        }

        #endregion

        #region " EVENT STREAM COMPLETED "

        /// <summary> Event queue for all listeners interested in EventStreamCompleted events. </summary>
        public event EventHandler<StreamStatusEventArgs> EventStreamCompleted;

        /// <summary> Raises the <see cref="EventStreamCompleted"/> event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnSendCompleted( StreamStatusEventArgs e )
        {
            var evt = EventStreamCompleted;
            evt?.Invoke( this, e );
        }

        /// <summary> Raises the <see cref="EventStreamCompleted"/> event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="status"> The status. </param>
        protected virtual void OnSendCompleted( System.Net.HttpStatusCode status )
        {
            this.OnSendCompleted( new StreamStatusEventArgs( status ) );
        }

        /// <summary> Executes the send event completed action. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="response"> The response. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void OnSendEventCompleted( IRestResponse response )
        {
            try
            {
                this.RateLimit.Parse( response );
            }
            catch ( Exception ex )
            {
                this.OnExceptionOccurred( new InvalidOperationException( "Failed parsing the response rate limit; Reported rate limits are in the exception Data.", ex ) );
            }
            finally
            {
                this.OnSendCompleted( response.StatusCode );
            }
        }

        /// <summary> Executes the send event completed action. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="bucketKey"> The key of the bucket receiving the events. </param>
        /// <param name="response">  The response. </param>
        /// <param name="request">   The request. </param>
        /// <param name="sentAsync"> True to sent asynchronous. </param>
        protected void OnSendEventCompleted( string activity, string bucketKey, IRestResponse response, IRestRequest request, bool sentAsync )
        {
            if ( ( int ) response.StatusCode == 429 )
            {
                this.RateLimit.Parse( response );
            }

            if ( Client.IsFailed( response.StatusCode, ExpectedStreamEventsStatusCodes() ) )
            {
                var ex = new RestApiException( $"{response.StatusCode} {activity} to {response.ResponseUri} ({this.AccessBucket( bucketKey )})", response, request );
                if ( sentAsync )
                {
                    this.OnExceptionOccurred( ex );
                }
                else
                {
                    throw ex;
                }
            }
            else
            {
                this.OnSendEventCompleted( response );
            }
        }

        #endregion

        #region " TIME STAMP "

        /// <summary> Gets or sets the epoch start time. </summary>
        /// <value> The epoch start date. </value>
        public static DateTimeOffset EpochStartTime { get; private set; } = new DateTimeOffset( new DateTime( 1970, 1, 1 ) );

        /// <summary> Converts a timestamp to an unix epoch time. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="timestamp"> The timestamp Date/Time. </param>
        /// <returns> The epoch. </returns>
        public static double ToUnixEpochTime( DateTime timestamp )
        {
            return 0.001d * timestamp.Subtract( EpochStartTime.DateTime ).TotalMilliseconds;
        }

        /// <summary> Converts a timestamp to an unix epoch time. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="timestamp"> The timestamp Date/Time. </param>
        /// <returns> The epoch. </returns>
        public static double ToUnixEpochTime( DateTimeOffset timestamp )
        {
            return 0.001d * timestamp.Subtract( EpochStartTime ).TotalMilliseconds;
        }

        /// <summary> Converts a timestamp to an ISO 8601. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="timestamp"> The timestamp Date/Time. </param>
        /// <returns> Timestamp as a String. </returns>
        public static string ToIso8601( DateTime timestamp )
        {
            return timestamp.ToString( "yyyy-MM-ddTHH:mm:ss.ffffZ" );
        }

        /// <summary> Converts a timestamp to an ISO 8601. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="timestamp"> The timestamp Date/Time. </param>
        /// <returns> Timestamp as a String. </returns>
        public static string ToIso8601( DateTimeOffset timestamp )
        {
            return timestamp.ToString( "o" );
        }

        #endregion

    }
}
