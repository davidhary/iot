// 
// Bucket Base Stream Task
// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;

using isr.IoT.InitialState.DispatcherExtensions;
using isr.IoT.InitialState.TaskExtensions;

namespace isr.IoT.InitialState
{

    public partial class BucketBase
    {

        #region " TASK MANAGEMENT "

        /// <summary> Reference to the awaiting task. </summary>
        /// <value> The awaiting task. </value>
        protected Task AwaitingTask { get; private set; }

        /// <summary> Reference to the Action task; this task status undergoes changes. </summary>
        /// <value> The action task. </value>
        protected Task ActionTask { get; private set; }

        /// <summary> Reference to the cancellation source. </summary>
        /// <value> The task cancellation source. </value>
        protected System.Threading.CancellationTokenSource TaskCancellationSource { get; private set; }

        /// <summary> Queries if a task is active. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> <c>true</c> if a task is active; otherwise <c>false</c> </returns>
        public bool IsTaskActive()
        {
            return this.ActionTask.IsTaskActive();
        }

        /// <summary> Waits for the task to idle. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="timeout"> The timeout. </param>
        public void AwaitTaskIdle( TimeSpan timeout )
        {
            if ( this.ActionTask is object )
                _ = this.ActionTask.Wait( timeout );
        }

        #endregion

        #region " STREAM TASK "

        /// <summary> Stream Task canceled. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private void StreamTaskCanceled()
        {
            this.OnStreamTaskEnded( TaskStatus.Canceled );
        }

        /// <summary> Starts the action task. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="taskAction"> The action to stream the entities, which calls
        /// <see cref="StreamEvents{T}(IEnumerable{T}, IEnumerable{DateTimeOffset}, int, string)"/>.
        /// </param>
        /// <returns> The awaiting task. </returns>
        private async Task AsyncAwaitTask( Action taskAction )
        {
            this.ActionTask = Task.Run( taskAction );
            await this.ActionTask; // Task.Run(streamEntitiesAction)
            try
            {
                this.ActionTask?.Wait();
                this.OnStreamTaskEnded( this.ActionTask is null ? TaskStatus.RanToCompletion : this.ActionTask.Status );
            }
            catch ( AggregateException ex )
            {
                this.OnExceptionOccurred( ex );
            }
            finally
            {
                this.TaskCancellationSource.Dispose();
            }
        }

        /// <summary> Starts Streaming the events. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="bucketKey">            The bucket key. </param>
        /// <param name="timeout">              The timeout. </param>
        /// <param name="streamEntitiesAction"> The action to stream the entities, which calls
        /// <see cref="StreamEvents{T}(IEnumerable{T}, IEnumerable{DateTimeOffset}, int, string)"/>.
        /// </param>
        public virtual void StartStreamEvents( string bucketKey, TimeSpan timeout, Action streamEntitiesAction )
        {
            if ( this.IsTaskActive() )
            {
                throw new InvalidOperationException( $"Stream task is {this.ActionTask.Status}" );
            }
            else
            {
                this.TaskCancellationSource = new System.Threading.CancellationTokenSource();
                _ = this.TaskCancellationSource.Token.Register( this.StreamTaskCanceled );
                this.TaskCancellationSource.CancelAfter( timeout );
                // the action class is created withing the Async/Await function
                this.AwaitingTask = this.AsyncAwaitTask( streamEntitiesAction );
            }
        }

        /// <summary> Event queue for all listeners interested in <see cref="StreamTaskEnded"/> events. </summary>
        public event EventHandler<TaskStatusEventArgs> StreamTaskEnded;

        /// <summary> Raises the <see cref="StreamTaskEnded"/> event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="status"> The task. </param>
        protected virtual void OnStreamTaskEnded( TaskStatus status )
        {
            this.OnTaskEnded( new TaskStatusEventArgs( status ) );
        }

        /// <summary> Raises the <see cref="StreamTaskEnded"/> event. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="e"> Event information to Task to registered event handlers. </param>
        protected virtual void OnTaskEnded( TaskStatusEventArgs e )
        {
            var evt = StreamTaskEnded;
            evt?.Invoke( this, e );
        }

        #endregion

        #region " STREAM ACTION "

        /// <summary>
        /// Streams a collection of entities using a multiple API calls each streaming a
        /// <paramref name="bufferSize"/> size of entities.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="entities">   The entities. </param>
        /// <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
        /// <param name="bufferSize"> The buffer size. </param>
        /// <param name="bucketKey">  The key of the bucket receiving the events. </param>
        public void StreamEvents<T>( IEnumerable<T> entities, IEnumerable<DateTime> timestamps, int bufferSize, string bucketKey )
        {
            var elementsQueue = new Queue<T>( entities );
            var timeStampsQueue = new Queue<DateTime>( timestamps );
            while ( elementsQueue.Any() )
            {
                try
                {
                    if ( this.TaskCancellationSource is object && this.TaskCancellationSource.Token.IsCancellationRequested )
                    {
                        break;
                    }
                    // wait if rate limit was hit or if the remaining request count is zero
                    _ = Dispatcher.CurrentDispatcher.Wait( this.RateLimit.PauseTimespan );
                    Dispatcher.CurrentDispatcher.DoEvents();
                    int n = Math.Min( bufferSize, elementsQueue.Count );
                    var elementList = new List<T>( elementsQueue.Take( n ) );
                    var timeList = new List<DateTime>( timeStampsQueue.Take( n ) );
                    this.StreamEvents( elementList, timeList, bucketKey, false );
                    // if no error, dequeue
                    while ( n > 0 )
                    {
                        _ = elementsQueue.Dequeue();
                        _ = timeStampsQueue.Dequeue();
                        n -= 1;
                    }

                    Dispatcher.CurrentDispatcher.DoEvents();
                }
                catch ( RestApiException ex )
                {
                    if ( ( int ) ex.StatusCode == RateLimit.RateLimitedRequestStatusCode && this.RateLimit.SuccessiveRateLimitCountOut > 0 )
                    {
                    }
                    // if request rate hit limit, try again unless the successive rate limit count out expired.
                    else
                    {
                        this.TaskCancellationSource?.Cancel();
                        this.OnExceptionOccurred( ex );
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Streams a collection of entities using a multiple API calls each streaming a
        /// <paramref name="bufferSize"/> size of entities.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="entities">   The entities. </param>
        /// <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
        /// <param name="bufferSize"> The buffer size. </param>
        /// <param name="bucketKey">  The key of the bucket receiving the events. </param>
        public void StreamEvents<T>( IEnumerable<T> entities, IEnumerable<DateTimeOffset> timestamps, int bufferSize, string bucketKey )
        {
            var elementsQueue = new Queue<T>( entities );
            var timeStampsQueue = new Queue<DateTimeOffset>( timestamps );
            while ( elementsQueue.Any() )
            {
                try
                {
                    if ( this.TaskCancellationSource is object && this.TaskCancellationSource.Token.IsCancellationRequested )
                    {
                        break;
                    }
                    // wait if rate limit was hit or if the remaining request count is zero
                    _ = Dispatcher.CurrentDispatcher.Wait( this.RateLimit.PauseTimespan );
                    Dispatcher.CurrentDispatcher.DoEvents();
                    int n = Math.Min( bufferSize, elementsQueue.Count );
                    var elementList = new List<T>( elementsQueue.Take( n ) );
                    var timeList = new List<DateTimeOffset>( timeStampsQueue.Take( n ) );
                    this.StreamEvents( elementList, timeList, bucketKey, false );
                    // if no error, dequeue
                    while ( n > 0 )
                    {
                        _ = elementsQueue.Dequeue();
                        _ = timeStampsQueue.Dequeue();
                        n -= 1;
                    }

                    Dispatcher.CurrentDispatcher.DoEvents();
                }
                catch ( RestApiException ex )
                {
                    if ( ( int ) ex.StatusCode == RateLimit.RateLimitedRequestStatusCode && this.RateLimit.SuccessiveRateLimitCountOut > 0 )
                    {
                    }
                    // if request rate hit limit, try again unless the successive rate limit count out expired.
                    else
                    {
                        this.TaskCancellationSource?.Cancel();
                        this.OnExceptionOccurred( ex );
                        break;
                    }
                }
            }
        }

        #endregion


    }
}
