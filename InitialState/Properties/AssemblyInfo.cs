﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.IoT.InitialState.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.IoT.InitialState.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.IoT.InitialState.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
