using System;
using System.Collections.Generic;
using System.Linq;

using RestSharp;

namespace isr.IoT.InitialState
{

    /// <summary> An initial state client. </summary>
        /// <remarks>
        /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2019-05-06 </para>
        /// </remarks>
    public partial class Client
    {

        #region " CONSTRUCTION AND CLEANUP "

        /// <summary>
        /// Creates an API Client for creating Initial State Buckets and sending events to the bucket.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="accessKey"> The access key. </param>
        public Client( string accessKey ) : base()
        {
            if ( string.IsNullOrEmpty( accessKey ) )
                throw new ArgumentException( "An access key is required" );
            this.AccessKey = accessKey;
            this.RestClient = new RestClient( BaseUrl ) { UserAgent = $"{My.MyLibrary.AssemblyProduct}/{My.MyProject.Application.Info.Version}" };
        }

        #endregion

        #region " END POINTS "

        /// <summary> The versions end point. </summary>
        public const string VersionsEndPoint = "/versions";

        /// <summary> The buckets end point. </summary>
        public const string BucketsEndPoint = "/buckets";

        /// <summary> The Events end point. </summary>
        public const string EventsEndPoint = "/events";

        /// <summary> Type of the JSON content. </summary>
        public const string JsonContentType = "application/json";

        #endregion

        #region " HEADERS "

        /// <summary> The access key header. </summary>
        public const string AccessKeyHeader = "X-IS-AccessKey";

        /// <summary> The bucket key header. </summary>
        public const string BucketKeyHeader = "X-IS-BucketKey";

        /// <summary> The content type header. </summary>
        public const string ContentTypeHeader = "Content-Type";

        /// <summary> The accept version header. </summary>
        public const string AcceptVersionHeader = "Accept-Version";

        #endregion

        #region " REST CLIENT "

        /// <summary> Gets or sets the access key. </summary>
        /// <value> The access key. </value>
        protected string AccessKey { get; private set; }

        /// <summary> Gets or sets the REST client. </summary>
        /// <value> The REST client. </value>
        protected RestClient RestClient { get; private set; }

        /// <summary> URL of the base API. </summary>
        public const string BaseUrl = "https://groker.init.st/api";

        /// <summary> Query if 'response' failed. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="statusCode"> The status code. </param>
        /// <returns> <c>true</c> if failed; otherwise <c>false</c> </returns>
        public static bool IsFailed( int statusCode )
        {
            return statusCode >= ( int ) System.Net.HttpStatusCode.MultipleChoices || statusCode < ( int ) System.Net.HttpStatusCode.OK;
        }

        /// <summary> Query if 'response' failed. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="statusCode">          The status code. </param>
        /// <param name="acceptedStatusCodes"> The accepted status codes. </param>
        /// <returns> <c>true</c> if failed; otherwise <c>false</c> </returns>
        public static bool IsFailed( int statusCode, int[] acceptedStatusCodes )
        {
            return acceptedStatusCodes is object && !acceptedStatusCodes.Contains( statusCode );
        }

        /// <summary> Query if 'response' failed. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <param name="statusCode">          The status code. </param>
        /// <param name="acceptedStatusCodes"> The accepted status codes. </param>
        /// <returns> <c>true</c> if failed; otherwise <c>false</c> </returns>
        public static bool IsFailed( System.Net.HttpStatusCode statusCode, System.Net.HttpStatusCode[] acceptedStatusCodes )
        {
            return acceptedStatusCodes is object && !acceptedStatusCodes.Contains( statusCode );
        }

        /// <summary> Gets or sets the API version. </summary>
        /// <value> The API version. </value>
        public static string ApiVersion { get; set; } = "~0";

        #endregion

    }
}
