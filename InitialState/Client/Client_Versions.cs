// 
// Versions Requests
// 
using System;
using System.Collections.Generic;
using System.Linq;

using RestSharp;
using RestSharp.Serialization.Json;

namespace isr.IoT.InitialState
{
    public partial class Client
    {

        /// <summary> Request versions response. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <exception cref="RestApiException"> Thrown if either API or <see cref="RestSharp"/> errors
        /// occurred. </exception>
        /// <returns> An IRestResponse. </returns>
        private IRestResponse RequestVersionsResponse()
        {
            var request = new RestRequest( VersionsEndPoint, Method.GET ) { JsonSerializer = new JsonSerializer() };
            string activity = $"requesting {VersionsEndPoint}";
            var response = this.RestClient.Execute( request );
            return Client.IsFailed( response.StatusCode, ExpectedRequestVersionStatusCodes() )
                ? throw new RestApiException( $"{response.StatusCode} {activity} from {response.ResponseUri}", response, request )
                : response;
        }

        /// <summary> Gets the expected request version status codes. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> The expected request version status codes. </returns>
        public static System.Net.HttpStatusCode[] ExpectedRequestVersionStatusCodes()
        {
            return new System.Net.HttpStatusCode[] { System.Net.HttpStatusCode.OK };
        }

        /// <summary> Request last version. </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        /// <returns> A Version. </returns>
        public Version RequestLastVersion()
        {
            var response = this.RequestVersionsResponse();
            var ser = new JsonSerializer();
            var l = ser.Deserialize<List<string>>( response );
            return new Version( l.Last() );
        }
    }
}
