
namespace isr.IoT.InitialState.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-27. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-27. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 0x300; // isr.Core.ProjectTraceEventId.InitialStateClient

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Initial State Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Initial State Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "isr.IoT.Initial.State";
    }
}
