﻿using System.Reflection;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle("RestSharp")]
[assembly: AssemblyDescription("Simple REST and HTTP API Client")]
[assembly: AssemblyCompany("RestSharp Community")]
[assembly: AssemblyProduct("RestSharp")]
[assembly: AssemblyCopyright("Copyright © 2013 RestSharp Community")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("106.6.9")]
