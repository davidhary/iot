﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Threading
Namespace DispatcherExtensions

    ''' <summary> Dispatcher extension methods. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Executes the events operation. </summary>
        ''' <remarks>
        ''' https://stackoverflow.com/questions/4502037/where-is-the-application-doevents-in-wpf. Well, I
        ''' just hit a case where I start work on a method that runs on the Dispatcher thread, and it
        ''' needs to block without blocking the UI Thread. Turns out that MSDN topic on the Dispatcher
        ''' Push Frame method explains how to implement a DoEvents() based on the Dispatcher itself:
        ''' https://goo.gl/WGFZEU
        ''' <code>
        ''' Imports System.Windows.Threading
        ''' Imports DispatcherExtensions
        ''' Dispatcher.CurrentDispatcher.DoEvents
        ''' </code>
        ''' <list type="bullet">Benchmarks<item>
        ''' 1000 iterations of Application Do Events:   7.8,  8.0, 11,  8.4 us </item><item>
        ''' 1000 iterations of Dispatcher Do Events:   57.6, 92.5, 93, 61.6 us </item></list>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="dispatcher"> The dispatcher. </param>
        <Extension>
        Public Sub DoEvents(ByVal dispatcher As Dispatcher)
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            Dim frame As New DispatcherFrame
            dispatcher.BeginInvoke(DispatcherPriority.Background, New DispatcherOperationCallback(AddressOf Methods.ExitFrame), frame)
            Dispatcher.PushFrame(frame)
        End Sub

        ''' <summary> Exit frame. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <param name="f"> references to the <see cref="DispatcherFrame"/>. </param>
        ''' <returns> An Object. </returns>
        Private Function ExitFrame(f As Object) As Object
            TryCast(f, DispatcherFrame).Continue = False
            Return Nothing
        End Function

        ''' <summary>
        ''' Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority.
        ''' </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="act">        The act. </param>
        <Extension>
        Public Sub Render(ByVal dispatcher As Dispatcher, ByVal act As Action)
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            dispatcher.Invoke(DispatcherPriority.Render, act)
        End Sub

        ''' <summary> Waits for the specified time to elapse. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="stopwatch">  The stopwatch. </param>
        ''' <param name="value">      The value. </param>
        ''' <returns> The elapsed time span including any time already elapsed on the stopwatch. </returns>
        <Extension()>
        Public Function Wait(ByVal dispatcher As Dispatcher, ByVal stopwatch As Stopwatch, ByVal value As TimeSpan) As TimeSpan
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            If stopwatch IsNot Nothing AndAlso value > TimeSpan.Zero Then
                If Not stopwatch.IsRunning Then stopwatch.Restart()
                value = value.Add(stopwatch.Elapsed)
                Do
                    dispatcher.DoEvents
                Loop Until stopwatch.Elapsed >= value
                Return stopwatch.Elapsed
            Else
                Return TimeSpan.Zero
            End If
        End Function

        ''' <summary> Waits for the specified time to elapse. </summary>
        ''' <remarks>
        ''' <list type="bullet">Benchmarks:<item>
        ''' 1000 iterations of 1ms:   1.10, 1.06ms </item><item>
        ''' 100 iterations of 10ms:  10.1, 10.07ms </item><item>
        ''' 10 iterations of 100ms: 100.05, 100.15ms </item></list>
        ''' </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="value">      The value. </param>
        ''' <returns> A TimeSpan. </returns>
        <Extension()>
        Public Function Wait(ByVal dispatcher As Dispatcher, ByVal value As TimeSpan) As TimeSpan
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            Dim stopwatch As Stopwatch = Stopwatch.StartNew
            Do
                dispatcher.DoEvents
            Loop Until stopwatch.Elapsed >= value
            Return stopwatch.Elapsed
        End Function

        ''' <summary> Waits while the stop watch is running until its elapsed time expires. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="stopwatch">  The stop watch. </param>
        ''' <param name="value">      The value. </param>
        ''' <returns> The elapsed <see cref="System.TimeSpan"/> </returns>
        <Extension()>
        Public Function LetElapse(ByVal dispatcher As Dispatcher, ByVal stopwatch As Stopwatch, ByVal value As TimeSpan) As TimeSpan
            If dispatcher Is Nothing Then Throw New ArgumentNullException(NameOf(dispatcher))
            If stopwatch IsNot Nothing AndAlso value > TimeSpan.Zero AndAlso stopwatch.IsRunning Then
                Do
                    dispatcher.DoEvents
                Loop Until stopwatch.Elapsed > value
                Return stopwatch.Elapsed
            Else
                Return TimeSpan.Zero
            End If
        End Function

    End Module

End Namespace

