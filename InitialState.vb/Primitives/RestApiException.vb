Imports System.Net

''' <summary> Exception throw if either API or <see cref="RestSharp"/> errors occurred. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/4/2019 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class RestApiException
    Inherits Exception

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub New(message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(message As String, innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="message">         The message. </param>
    ''' <param name="statusCode">      The status code. </param>
    ''' <param name="firstParameter">  The first parameter of the
    '''                                <see cref="RestSharp.IRestRequest">request</see> body. </param>
    ''' <param name="responseContent"> The <see cref="RestSharp.IRestResponse">response</see>
    '''                                content. </param>
    Public Sub New(ByVal message As String, ByVal statusCode As HttpStatusCode, ByVal firstParameter As String, ByVal responseContent As String)
        MyBase.New(message)
        Me.StatusCode = statusCode
        Me.RequestFirstParameter = firstParameter
        Me.ResponseContent = responseContent
    End Sub

    ''' <summary> Validated response. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="response"> The <see cref="RestSharp.IRestResponse">response</see>. </param>
    ''' <returns> A RestSharp.IRestResponse. </returns>
    Private Shared Function ValidatedResponse(ByVal response As RestSharp.IRestResponse) As RestSharp.IRestResponse
        If response Is Nothing Then Throw New ArgumentNullException(NameOf(response))
        Return response
    End Function

    ''' <summary>
    ''' Extracts the first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="request"> The <see cref="RestSharp.IRestRequest">request</see>. </param>
    ''' <returns>
    ''' The extracted first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
    ''' </returns>
    Private Shared Function ExtractRequestBodyFirstParameter(ByVal request As RestSharp.IRestRequest) As String
        If request Is Nothing Then Throw New ArgumentNullException(NameOf(request))
        Dim result As String = "<empty>"
        Dim firstParameter As RestSharp.Parameter = request.Parameters.FirstOrDefault(Function(x) x.Type = RestSharp.ParameterType.RequestBody)
        If firstParameter IsNot Nothing Then result = firstParameter.ToString
        Return result
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="message">  The message. </param>
    ''' <param name="response"> The <see cref="RestSharp.IRestResponse">response</see>. </param>
    ''' <param name="request">  The <see cref="RestSharp.IRestRequest">request</see>. </param>
    Public Sub New(ByVal message As String, ByVal response As RestSharp.IRestResponse, ByVal request As RestSharp.IRestRequest)
        Me.New(message, RestApiException.ValidatedResponse(response).ErrorException, response.StatusCode,
               RestApiException.ExtractRequestBodyFirstParameter(request), response.Content)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="statusCode">      The status code. </param>
    ''' <param name="firstParameter">  The first parameter of the
    '''                                <see cref="RestSharp.IRestRequest">request</see> body. </param>
    ''' <param name="responseContent"> The <see cref="RestSharp.IRestResponse">response</see>
    '''                                content. </param>
    Public Sub New(ByVal statusCode As HttpStatusCode, ByVal firstParameter As String, ByVal responseContent As String)
        MyBase.New()
        Me.StatusCode = statusCode
        Me.RequestFirstParameter = firstParameter
        Me.ResponseContent = responseContent
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="message">         The message. </param>
    ''' <param name="exception">       The exception. </param>
    ''' <param name="statusCode">      The status code. </param>
    ''' <param name="firstParameter">  The first parameter of the
    '''                                <see cref="RestSharp.IRestRequest">request</see> body. </param>
    ''' <param name="responseContent"> The <see cref="RestSharp.IRestResponse">response</see>
    '''                                content. </param>
    Public Sub New(ByVal message As String, ByVal exception As Exception,
                   ByVal statusCode As HttpStatusCode, ByVal firstParameter As String, ByVal responseContent As String)
        MyBase.New(message, exception)
        Me.StatusCode = statusCode
        Me.RequestFirstParameter = firstParameter
        Me.ResponseContent = responseContent
    End Sub

    ''' <summary> Gets or sets the status code. </summary>
    ''' <value> The status code. </value>
    Public Property StatusCode() As HttpStatusCode

    ''' <summary>
    ''' Gets or sets the first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
    ''' </summary>
    ''' <value>
    ''' The first parameter of the <see cref="RestSharp.IRestRequest">request</see> body.
    ''' </value>
    Public Property RequestFirstParameter() As String

    ''' <summary>
    ''' Gets or sets the <see cref="RestSharp.IRestResponse">response</see> content.
    ''' </summary>
    ''' <value> The <see cref="RestSharp.IRestResponse">response</see> content. </value>
    Public Property ResponseContent() As String

End Class

