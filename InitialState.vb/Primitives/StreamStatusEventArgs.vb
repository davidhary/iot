﻿Imports RestSharp

''' <summary> Additional information for Stream status events. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/9/2019 </para>
''' </remarks>
Public Class StreamStatusEventArgs
    Inherits System.EventArgs

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="status"> The status. </param>
    Public Sub New(ByVal status As Net.HttpStatusCode)
        MyBase.New
        Me.Status = status
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="response"> The Stream. </param>
    Public Sub New(ByVal response As RestSharp.RestResponse)
        MyBase.New
        Me.Status = If(response Is Nothing, Net.HttpStatusCode.OK, response.StatusCode)
    End Sub

    ''' <summary> Gets the okay. </summary>
    ''' <value> The okay. </value>
    Public Shared ReadOnly Property Okay As StreamStatusEventArgs
        Get
            Return New StreamStatusEventArgs(Net.HttpStatusCode.OK)
        End Get
    End Property

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public Property Status As Net.HttpStatusCode

End Class

