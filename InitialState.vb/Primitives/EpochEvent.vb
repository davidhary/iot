﻿''' <summary> An event using Unit Epoch time as a timestamp. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/4/2019 </para>
''' </remarks>
Friend Class EpochEvent

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the key. </summary>
    ''' <value> The key. </value>
    Public Property key() As String
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property value() As String
#Enable Warning IDE1006 ' Naming Styles

#Disable Warning IDE1006 ' Naming Styles
    ''' <summary> Gets or sets the epoch. </summary>
    ''' <value> The epoch. </value>
    Public Property epoch() As Double
#Enable Warning IDE1006 ' Naming Styles

End Class
