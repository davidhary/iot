Imports System.Runtime.CompilerServices
Namespace StringExtensions

    ''' <summary> Includes 'include' extensions for <see cref="String">String</see>. </summary>
    ''' <remarks> (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 04/09/2009, 1.1.3386.x. </para></remarks>
    Public Module Methods

        ''' <summary> Returns true if the string includes the specified characters. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     The string that is being searched. </param>
        ''' <param name="characters"> The characters to search for. </param>
        ''' <returns>
        ''' <c>True</c> if the string includes the specified characters; otherwise, <c>False</c>.
        ''' </returns>
        <Extension()>
        Public Function IncludesCharacters(ByVal source As String, ByVal characters As String) As Boolean
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If characters Is Nothing Then Throw New ArgumentNullException(NameOf(characters))
            Dim expression As String = $"[{characters}]"
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(expression,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Return r.IsMatch(source)
        End Function

        ''' <summary> Enumerates matching characters in this collection. </summary>
        ''' <remarks> David, 10/27/2020. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="source">     The string that is being searched. </param>
        ''' <param name="characters"> The characters to search for. </param>
        ''' <returns>
        ''' An enumerator that allows for each to be used to process matching characters in this
        ''' collection.
        ''' </returns>
        <Extension()>
        Public Function MatchingCharacters(ByVal source As String, ByVal characters As String) As IEnumerable(Of String)
            If source Is Nothing Then Throw New ArgumentNullException(NameOf(source))
            If characters Is Nothing Then Throw New ArgumentNullException(NameOf(characters))
            Dim expression As String = $"[{characters}]"
            Dim r As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex(expression,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Dim matches As System.Text.RegularExpressions.MatchCollection = r.Matches(source)
            If matches.Count = 0 Then
                Return Array.Empty(Of String)()
            Else
                Dim l As New List(Of String)
                For Each m As System.Text.RegularExpressions.Match In matches
                    l.Add(m.Value)
                Next
                Return l
            End If
        End Function

    End Module
End Namespace

