﻿Imports RestSharp

''' <summary> A rate limit. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/8/2019 </para>
''' </remarks>
Public Class RateLimit

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Limit = RateLimit.AccountPlanRateLimit * RateLimit.RateLimitSamplePeriod
        Me.Remaining = Me.Limit
        Me.Reset = RateLimit.RateLimitSamplePeriod
        Me.IsHitRateLimit = False
        Me.RetryAfterTimespan = TimeSpan.Zero
        Me.Status = Net.HttpStatusCode.OK
    End Sub

    ''' <summary> Gets the account plan rate limit. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The account plan rate limit. </value>
    Public Shared Property AccountPlanRateLimit As Integer = 3

    ''' <summary> Gets the rate limit sample period in seconds. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The rate limit sample period. </value>
    Public Shared Property RateLimitSamplePeriod As Integer = 10

    ''' <summary> Parses the given response. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="response"> The response. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2200:RethrowToPreserveStackDetails")>
    Public Sub Parse(ByVal response As IRestResponse)
        If response Is Nothing Then Throw New ArgumentNullException(NameOf(response))
        Dim l As List(Of Parameter) = Nothing
        Try
            l = response.Headers.ToList
            Me.Status = response.StatusCode
            Dim headerValue As String = String.Empty
            headerValue = l.Find(Function(x) x.Name = RateLimit.LimitHeader)?.Value.ToString
            If Not String.IsNullOrEmpty(headerValue) Then Me.Limit = Integer.Parse(headerValue)

            headerValue = l.Find(Function(x) x.Name = RateLimit.RemainingHeader)?.Value.ToString
            If Not String.IsNullOrEmpty(headerValue) Then Me.Remaining = Integer.Parse(headerValue)

            headerValue = l.Find(Function(x) x.Name = RateLimit.ResetHeader)?.Value.ToString
            If Not String.IsNullOrEmpty(headerValue) Then Me.Reset = Long.Parse(headerValue)

            Me._IsHitRateLimit = response.StatusCode = RateLimit.RateLimitedRequestStatusCode
            If Me.IsHitRateLimit Then
                headerValue = l.Find(Function(x) x.Name = RateLimit.RetryAfterHeader)?.Value.ToString
                If Not String.IsNullOrEmpty(headerValue) Then Me.UpdateRetry(Integer.Parse(headerValue))
                Me._SuccessiveRateLimitCountOut -= 1
            Else
                Me._RetryAfterTimespan = TimeSpan.Zero
                Me._SuccessiveRateLimitCountOut = RateLimit.DefaultSuccessiveRateLimitCountOut
            End If
        Catch ex As Exception
            If l IsNot Nothing Then
                For Each p As Parameter In l
                    ex.Data.Add($"{p.Name}=", p.Value)
                Next
            End If
            Throw ex
        End Try
    End Sub

    ''' <summary> The limit header. </summary>
    Private Const LimitHeader As String = "X-RateLimit-Limit"

    ''' <summary> The remaining header. </summary>
    Private Const RemainingHeader As String = "X-RateLimit-Remaining"

    ''' <summary> The reset header. </summary>
    Private Const ResetHeader As String = "X-RateLimit-Reset"

    ''' <summary> The retry after header. </summary>
    Private Const RetryAfterHeader As String = "Retry-After"

    ''' <summary> The rate limited request status code. </summary>
    Public Const RateLimitedRequestStatusCode As Integer = 429 ' too many requests

    ''' <summary> Gets the status. </summary>
    ''' <value> The status. </value>
    Public Property Status As Net.HttpStatusCode

    ''' <summary> Gets the rate limit in requests per 10 second interval. </summary>
    ''' <remarks>
    ''' API Header: <c>X-RateLimit-Limit</c><para>
    ''' This Is the current number of requests per 10 second interval. This value is based on the
    ''' account plan and accessKey rights</para>
    ''' </remarks>
    ''' <value> The rate limit in requests per 10 second interval. </value>
    Public Property Limit As Integer

    ''' <summary>
    ''' Gets the number of requests left from the <see cref="Limit"/> during a 10 second interval.
    ''' </summary>
    ''' <remarks>
    ''' API Header: <c>X-RateLimit-Remaining</c><para>
    ''' The number of requests left from the <see cref="Limit"/> during a 10 second interval</para>
    ''' </remarks>
    ''' <value>
    ''' The number of requests left from the <see cref="Limit"/> during a 10 second interval.
    ''' </value>
    Public Property Remaining As Integer

    ''' <summary>
    ''' Gets the Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back
    ''' to the value of <see cref="Limit"/>.
    ''' </summary>
    ''' <remarks>
    ''' API Header: <c>X-RateLimit-Reset</c><para>
    ''' The Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back to
    ''' the value of <see cref="Limit"/></para>
    ''' </remarks>
    ''' <value>
    ''' The Unix epoch in seconds indicating when the <see cref="Remaining"/> will be reset back to
    ''' the value of <see cref="Limit"/>.
    ''' </value>
    Public Property Reset As Long

    ''' <summary>
    ''' Gets time to wait until the <see cref="Remaining"/> will be reset back to the value of
    ''' <see cref="Limit"/>.
    ''' </summary>
    ''' <value> The reset timespan. </value>
    Public ReadOnly Property ResetTimespan As TimeSpan
        Get
            Return Me.ResetTime.Subtract(DateTimeOffset.Now)
        End Get
    End Property

    ''' <summary> Gets the <see cref="Reset"/> UTC time. </summary>
    ''' <value> The <see cref="Reset"/> UTC time. </value>
    Public ReadOnly Property ResetTime As DateTimeOffset
        Get
            Return BucketBase.EpochStartTime.AddSeconds(Me.Reset)
        End Get
    End Property

    ''' <summary> Returns <c>True</c> if the response failed hitting the rate limit. </summary>
    ''' <value>
    ''' <c>True</c> if the response failed hitting the rate limit; <c>False</c> otherwise.
    ''' </value>
    Public ReadOnly Property IsHitRateLimit As Boolean

    ''' <summary>
    ''' Updates the number of seconds requesting code should wait before retrying the request.
    ''' </summary>
    ''' <remarks>
    ''' API Header: <c>Retry-After</c><para>
    ''' This number of seconds requesting code should wait before retrying the request</para><para>
    ''' This header Is only returned during a rate limited request represented by an HTTP 429 Status
    ''' Code.
    ''' </para>
    ''' </remarks>
    ''' <param name="retryDelay"> The retry delay. This number of seconds requesting code should wait
    '''                           before retrying the request. </param>
    Public Sub UpdateRetry(ByVal retryDelay As Long)
        Me._RetryAfterTimespan = If(retryDelay > 0, TimeSpan.FromSeconds(retryDelay), TimeSpan.Zero)
    End Sub

    ''' <summary> Gets the <see cref="RetryAfter"/> timespan relative to the current time. </summary>
    ''' <value> The <see cref="RetryAfter"/> timespan relative to the current time. </value>
    Public ReadOnly Property RetryAfterTimespan As TimeSpan

    ''' <summary> Gets the <see cref="RetryAfter"/> UTC time. </summary>
    ''' <value> The <see cref="RetryAfter"/> UTC time. </value>
    Public ReadOnly Property RetryAfterTime As DateTimeOffset
        Get
            Return DateTimeOffset.Now.Add(Me.RetryAfterTimespan)
        End Get
    End Property

    ''' <summary>
    ''' Gets an indications if a hold is required, each as due to hitting rate limit or reaching the
    ''' request count <see cref="Limit"/>
    ''' </summary>
    ''' <value> The indication that a pause is required. </value>
    Public ReadOnly Property IsPauseRequired As Boolean
        Get
            Return Me.IsHitRateLimit OrElse Me.Remaining <= 0
        End Get
    End Property

    ''' <summary>
    ''' Gets the time to wait after <see cref="Reset"/> or <see cref="IsHitRateLimit"/> before
    ''' streaming new data.
    ''' </summary>
    ''' <value> The pause timespan. </value>
    Public ReadOnly Property PauseTimespan As TimeSpan
        Get
            If Me.IsHitRateLimit Then
                Return Me.RetryAfterTimespan
            ElseIf Me.Remaining <= 0 Then
                Return Me.ResetTimespan
            Else
                Return TimeSpan.Zero
            End If
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the rate limit count out. This value restarts on each successful stream. The
    ''' value decrements each time a rate limit is hit. It is used to raise an exception after a
    ''' stream of successive rate limits.
    ''' </summary>
    ''' <value> The rate limit count out. </value>
    Public ReadOnly Property SuccessiveRateLimitCountOut As Integer

    ''' <summary> Gets or sets the default successive rate limit count out. </summary>
    ''' <value> The default successive rate limit count out. </value>
    Public Shared Property DefaultSuccessiveRateLimitCountOut As Integer = 10

End Class