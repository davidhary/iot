﻿Imports System.Threading.Tasks

''' <summary> Additional information for task status events. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/9/2019 </para>
''' </remarks>
Public Class TaskStatusEventArgs
    Inherits System.EventArgs

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="status"> The status. </param>
    Public Sub New(ByVal status As TaskStatus)
        MyBase.New
        Me.Status = status
    End Sub

    ''' <summary> Gets the event arguments for the created status. </summary>
    ''' <value> The event arguments for the created status. </value>
    Public Shared ReadOnly Property Created As TaskStatusEventArgs
        Get
            Return New TaskStatusEventArgs(TaskStatus.Created)
        End Get
    End Property

    ''' <summary> Gets or sets the status. </summary>
    ''' <value> The status. </value>
    Public Property Status As TaskStatus

End Class

