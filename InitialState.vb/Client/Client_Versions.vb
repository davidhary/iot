'
' Versions Requests
' 
Imports RestSharp
Imports RestSharp.Serialization.Json

Partial Public Class Client

    ''' <summary> Request versions response. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="RestApiException"> Thrown if either API or <see cref="RestSharp"/> errors
    '''                                     occurred. </exception>
    ''' <returns> An IRestResponse. </returns>
    Private Function RequestVersionsResponse() As IRestResponse
        Dim request As New RestRequest(Client.VersionsEndPoint, Method.GET) With {.JsonSerializer = New JsonSerializer()}
        Dim activity As String = $"requesting {Client.VersionsEndPoint}"
        Dim response As IRestResponse = Me.RestClient.Execute(request)
        If Client.IsFailed(response.StatusCode, Client.ExpectedRequestVersionStatusCodes) Then
            Throw New RestApiException($"{response.StatusCode} {activity} from {response.ResponseUri}", response, request)
        End If
        Return response
    End Function

    ''' <summary> Gets the expected request version status codes. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> The expected request version status codes. </returns>
    Public Shared Function ExpectedRequestVersionStatusCodes() As Net.HttpStatusCode()
        Return New Net.HttpStatusCode() {Net.HttpStatusCode.OK}
    End Function

    ''' <summary> Request last version. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> A Version. </returns>
    Public Function RequestLastVersion() As Version
        Dim response As IRestResponse = Me.RequestVersionsResponse()
        Dim ser As New JsonSerializer
        Dim l As List(Of String) = ser.Deserialize(Of List(Of String))(response)
        Return New Version(l.Last)
    End Function

End Class

