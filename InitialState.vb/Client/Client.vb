Imports System.Configuration

Imports RestSharp

''' <summary> An initial state client. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/6/2019 </para>
''' </remarks>
Public Class Client

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary>
    ''' Creates an API Client for creating Initial State Buckets and sending events to the bucket.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="accessKey"> The access key. </param>
    Public Sub New(ByVal accessKey As String)
        MyBase.New()
        If String.IsNullOrEmpty(accessKey) Then Throw New ArgumentException("An access key is required")
        Me.AccessKey = accessKey
        Me._RestClient = New RestClient(Client.BaseUrl) With {.UserAgent = $"{My.MyLibrary.AssemblyProduct}/{My.Application.Info.Version}"}
    End Sub

#End Region

#Region " END POINTS "

    ''' <summary> The versions end point. </summary>
    Public Const VersionsEndPoint As String = "/versions"

    ''' <summary> The buckets end point. </summary>
    Public Const BucketsEndPoint As String = "/buckets"

    ''' <summary> The Events end point. </summary>
    Public Const EventsEndPoint As String = "/events"

    ''' <summary> Type of the JSON content. </summary>
    Public Const JsonContentType As String = "application/json"

#End Region

#Region " HEADERS "

    ''' <summary> The access key header. </summary>
    Public Const AccessKeyHeader As String = "X-IS-AccessKey"

    ''' <summary> The bucket key header. </summary>
    Public Const BucketKeyHeader As String = "X-IS-BucketKey"

    ''' <summary> The content type header. </summary>
    Public Const ContentTypeHeader As String = "Content-Type"

    ''' <summary> The accept version header. </summary>
    Public Const AcceptVersionHeader As String = "Accept-Version"

#End Region

#Region " REST CLIENT "

    ''' <summary> Gets or sets the access key. </summary>
    ''' <value> The access key. </value>
    Protected ReadOnly Property AccessKey As String

    ''' <summary> Gets or sets the REST client. </summary>
    ''' <value> The REST client. </value>
    Protected ReadOnly Property RestClient As RestClient

    ''' <summary> URL of the base API. </summary>
    Public Const BaseUrl As String = "https://groker.init.st/api"

    ''' <summary> Query if 'response' failed. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="statusCode"> The status code. </param>
    ''' <returns> <c>true</c> if failed; otherwise <c>false</c> </returns>
    Public Shared Function IsFailed(ByVal statusCode As Integer) As Boolean
        Return statusCode >= Net.HttpStatusCode.MultipleChoices OrElse statusCode < Net.HttpStatusCode.OK
    End Function

    ''' <summary> Query if 'response' failed. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="statusCode">          The status code. </param>
    ''' <param name="acceptedStatusCodes"> The accepted status codes. </param>
    ''' <returns> <c>true</c> if failed; otherwise <c>false</c> </returns>
    Public Shared Function IsFailed(ByVal statusCode As Integer, ByVal acceptedStatusCodes As Integer()) As Boolean
        Return acceptedStatusCodes IsNot Nothing AndAlso Not acceptedStatusCodes.Contains(statusCode)
    End Function

    ''' <summary> Gets or sets the API version. </summary>
    ''' <value> The API version. </value>
    Public Shared Property ApiVersion() As String = "~0"

#End Region

End Class
