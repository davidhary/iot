Imports System.Configuration

Imports RestSharp
Imports RestSharp.Serialization.Json

''' <summary> An initial state bucket base class. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/10/2019 </para>
''' </remarks>
Public MustInherit Class BucketBase
    Inherits Client
    Implements IDisposable

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary>
    ''' Creates an API Client for creating Initial State Buckets and sending events to the bucket.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="accessKey"> The access key. </param>
    Protected Sub New(ByVal accessKey As String)
        MyBase.New(accessKey)
        Me._RateLimit = New RateLimit()
    End Sub

    ''' <summary> Gets or sets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Protected ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.TaskCancellationSource?.Cancel()
                    If Me.ActionTask IsNot Nothing Then Me._ActionTask.Dispose() : Me._ActionTask = Nothing
                    If Me.TaskCancellationSource IsNot Nothing Then Me._TaskCancellationSource.Dispose() : Me._TaskCancellationSource = Nothing
                End If
            End If
        Catch
            Throw
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " BUCKET "

    ''' <summary> The illegal bucket key characters. </summary>
    Public Const IllegalBucketKeyCharacters As String = "/\\:*?""<>|."

    ''' <summary> Creates a bucket key using GUID. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> The new bucket key. </returns>
    Public Shared Function CreateBucketKey() As String
        Return Guid.NewGuid().ToString("N")
    End Function

    ''' <summary> Access bucket. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="bucketKey"> the bucket key associated with this instantiation. </param>
    ''' <returns> A String. </returns>
    Protected Function AccessBucket(ByVal bucketKey As String) As String
        Return $"{Me.AccessKey.Substring(0, 4)}...{Me.AccessKey.Substring(Me.AccessKey.Length - 5, 4)}:{bucketKey}"
    End Function

    ''' <summary> Create a bucket to receive events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="name"> The name of the bucket. </param>
    ''' <returns>
    ''' A <see cref="Tuple(Of Integer, String)"/> consisting of the status code and bucket key.
    ''' </returns>
    Public Function CreateBucket(ByVal name As String) As Tuple(Of Net.HttpStatusCode, String)
        Return Me.CreateBucket(BucketBase.CreateBucketKey, name, Nothing)
    End Function

    ''' <summary> Create a bucket to receive events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="key">  The unique key of the bucket in the scope of the Access Key. </param>
    ''' <param name="name"> The name of the bucket. </param>
    ''' <returns>
    ''' A <see cref="Tuple(Of Integer, String)"/> consisting of the status code and bucket key.
    ''' </returns>
    Public Function CreateBucket(ByVal key As String, ByVal name As String) As Tuple(Of Net.HttpStatusCode, String)
        Return Me.CreateBucket(key, name, Nothing)
    End Function

    ''' <summary> Create a bucket to receive events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="RestApiException">          Thrown if either API or <see cref="RestSharp"/>
    '''                                              errors occurred. </exception>
    ''' <param name="key">  The unique key of the bucket in the scope of the Access Key. </param>
    ''' <param name="name"> The name of the bucket. </param>
    ''' <param name="tags"> An array of string tags to attach to the bucket upon creation; could be
    '''                     empty. </param>
    ''' <returns>
    ''' A <see cref="Tuple(Of Integer, String)"/> consisting of the status code and bucket key.
    ''' </returns>
    Public Function CreateBucket(ByVal key As String, ByVal name As String, ByVal tags As String()) As Tuple(Of Net.HttpStatusCode, String)
        If StringExtensions.IncludesCharacters(key, BucketBase.IllegalBucketKeyCharacters) Then
            Throw New InvalidOperationException($"Bucket key {key} must not include any of {BucketBase.IllegalBucketKeyCharacters}")
        End If
        Dim request As New RestRequest(Client.BucketsEndPoint, Method.POST)
        request.AddHeader(Client.AccessKeyHeader, Me.AccessKey)
        request.AddHeader(Client.ContentTypeHeader, Client.JsonContentType)
        request.AddHeader(Client.AcceptVersionHeader, Client.ApiVersion)
        request.JsonSerializer = New JsonSerializer()
        If tags Is Nothing Then
            request.AddJsonBody(New With {Key .bucketName = name, Key .bucketKey = key})
        Else
            request.AddJsonBody(New With {Key .bucketName = name, Key .bucketKey = key, Key tags})
        End If
        Dim response As IRestResponse = Me.RestClient.Execute(request)
        If Client.IsFailed(response.StatusCode, BucketBase.ExpectedCreateBucketStatusCodes) Then
            Throw New RestApiException($"{response.StatusCode} creating bucket {response.ResponseUri}/{Me.AccessBucket(key)}", response, request)
        End If
        Return New Tuple(Of Net.HttpStatusCode, String)(response.StatusCode, key)
    End Function

    ''' <summary> Gets the expected status codes for creating a bucket. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> The expected status codes for creating a bucket. </returns>
    Public Shared Function ExpectedCreateBucketStatusCodes() As Net.HttpStatusCode()
        Return New Net.HttpStatusCode() {Net.HttpStatusCode.OK, Net.HttpStatusCode.Created, Net.HttpStatusCode.NoContent}
    End Function

#End Region

#Region " EXCEPTION HANDLERS "

    ''' <summary> The exception occurred. </summary>
    Public Event ExceptionOccurred As EventHandler(Of IO.ErrorEventArgs)

    ''' <summary> Raises the error event. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnExceptionOccurred(ByVal e As System.IO.ErrorEventArgs)
        Dim evt As EventHandler(Of IO.ErrorEventArgs) = Me.ExceptionOccurredEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the error event. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="exception"> The exception. </param>
    Protected Sub OnExceptionOccurred(ByVal exception As Exception)
        Me.OnExceptionOccurred(New IO.ErrorEventArgs(exception))
    End Sub

#End Region

End Class
