'
' Bucket Base Stream
' 
Imports System.Reflection
Imports RestSharp
Imports RestSharp.Serialization.Json

Partial Public Class BucketBase

#Region " STREAM EVENTS "

    ''' <summary> Gets or sets the rate limit. </summary>
    ''' <value> The rate limit. </value>
    Public Property RateLimit As RateLimit

    ''' <summary> Streams a single event as key, value and timestamp. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="key">       the key to associate with the <paramref name="value"/> of the event. </param>
    ''' <param name="value">     value of the event. </param>
    ''' <param name="timestamp"> The timestamp to use for this event. </param>
    ''' <param name="bucketKey"> The key of the bucket receiving the events. </param>
    ''' <param name="sendAsync"> <c>True</c> to stream the event without waiting for confirmed
    '''                          response; otherwise, <c>false</c>. </param>
    Public Sub StreamEvent(ByVal key As String, ByVal value As String, ByVal timestamp As DateTime, ByVal bucketKey As String, ByVal sendAsync As Boolean)
        Me.StreamEvents(New Dictionary(Of String, String) From {{key, value}}, timestamp, bucketKey, sendAsync)
    End Sub

    ''' <summary> Streams a collection of entities using a single API call. </summary>
    ''' <remarks>
    ''' An <see cref="RestApiException"/> might occur if the stream hit the event limiting rate.
    ''' </remarks>
    ''' <param name="entities">   The entities. </param>
    ''' <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
    ''' <param name="bucketKey">  The key of the bucket receiving the events. </param>
    ''' <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
    '''                           response;
    '''                                                     otherwise, <c>false</c>. </param>
    Public MustOverride Sub StreamEvents(Of T)(ByVal entities As IEnumerable(Of T), ByVal timestamps As IEnumerable(Of DateTime), ByVal bucketKey As String, ByVal sendAsync As Boolean)

    ''' <summary>
    ''' Streams a single entity as a set of events each representing a property name (as a key) and
    ''' value.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="entity">    The entity. </param>
    ''' <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
    ''' <param name="bucketKey"> the key of the bucket receiving the events. </param>
    ''' <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
    '''                          response;
    '''                                                    otherwise, <c>false</c>. </param>
    Public MustOverride Sub StreamEvents(Of T)(ByVal entity As T, ByVal timestamp As DateTime, ByVal bucketKey As String, ByVal sendAsync As Boolean)

    ''' <summary> Streams a single event as key, value and timestamp. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="key">       the key to associate with the <paramref name="value"/> of the event. </param>
    ''' <param name="value">     value of the event. </param>
    ''' <param name="timestamp"> The timestamp to use for this event. </param>
    ''' <param name="bucketKey"> The key of the bucket receiving the events. </param>
    ''' <param name="sendAsync"> <c>True</c> to stream the event without waiting for confirmed
    '''                                                   response;
    '''                                                   otherwise, <c>false</c>. </param>
    Public Sub StreamEvent(ByVal key As String, ByVal value As String, ByVal timestamp As DateTimeOffset, ByVal bucketKey As String, ByVal sendAsync As Boolean)
        Me.StreamEvents(New Dictionary(Of String, String) From {{key, value}}, timestamp, bucketKey, sendAsync)
    End Sub

    ''' <summary>
    ''' Streams a collection of entities using a multiple API calls each streaming a
    ''' <paramref name="bufferSize"/> size of entities.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="entities">   The entities. </param>
    ''' <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
    ''' <param name="bucketKey">  The key of the bucket receiving the events. </param>
    ''' <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
    '''                                                     response;
    '''                                                     otherwise, <c>false</c>. </param>
    Public MustOverride Sub StreamEvents(Of T)(ByVal entities As IEnumerable(Of T), ByVal timestamps As IEnumerable(Of DateTimeOffset), ByVal bucketKey As String, ByVal sendAsync As Boolean)

    ''' <summary>
    ''' Streams a collection of entities using a multiple API calls each streaming a
    ''' <paramref name="bufferSize"/> size of entities.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="entity">    The entity. </param>
    ''' <param name="timestamp"> The timestamp to use for this event. </param>
    ''' <param name="bucketKey"> The key of the bucket receiving the events. </param>
    ''' <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
    '''                                                   response;
    '''                                                   otherwise, <c>false</c>. </param>
    Public MustOverride Sub StreamEvents(Of T)(ByVal entity As T, ByVal timestamp As DateTimeOffset, ByVal bucketKey As String, ByVal sendAsync As Boolean)

    ''' <summary> Gets the expected status codes for streaming events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> The expected status codes for streaming events. </returns>
    Public Shared Function ExpectedStreamEventsStatusCodes() As Net.HttpStatusCode()
        Return New Net.HttpStatusCode() {Net.HttpStatusCode.OK, Net.HttpStatusCode.Created, Net.HttpStatusCode.NoContent}
    End Function

#End Region

#Region " EVENT STREAM COMPLETED "

    ''' <summary> Event queue for all listeners interested in EventStreamCompleted events. </summary>
    Public Event EventStreamCompleted As EventHandler(Of StreamStatusEventArgs)

    ''' <summary> Raises the <see cref="EventStreamCompleted"/> event. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnSendCompleted(ByVal e As StreamStatusEventArgs)
        Dim evt As EventHandler(Of StreamStatusEventArgs) = Me.EventStreamCompletedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Raises the <see cref="EventStreamCompleted"/> event. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="status"> The status. </param>
    Protected Overridable Sub OnSendCompleted(ByVal status As Net.HttpStatusCode)
        Me.OnSendCompleted(New StreamStatusEventArgs(status))
    End Sub

    ''' <summary> Executes the send event completed action. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="response"> The response. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub OnSendEventCompleted(ByVal response As IRestResponse)
        Try
            Me.RateLimit.Parse(response)
        Catch ex As Exception
            Me.OnExceptionOccurred(New InvalidOperationException("Failed parsing the response rate limit; Reported rate limits are in the exception Data.", ex))
        Finally
            Me.OnSendCompleted(response.StatusCode)
        End Try
    End Sub

    ''' <summary> Executes the send event completed action. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="activity">  The activity. </param>
    ''' <param name="bucketKey"> The key of the bucket receiving the events. </param>
    ''' <param name="response">  The response. </param>
    ''' <param name="request">   The request. </param>
    ''' <param name="sentAsync"> True to sent asynchronous. </param>
    Protected Sub OnSendEventCompleted(ByVal activity As String, ByVal bucketKey As String,
                                       ByVal response As IRestResponse, ByVal request As IRestRequest, ByVal sentAsync As Boolean)
        If response.StatusCode = 429 Then
            Me.RateLimit.Parse(response)
        End If
        If Client.IsFailed(response.StatusCode, BucketBase.ExpectedStreamEventsStatusCodes) Then
            Dim ex As New RestApiException($"{response.StatusCode} {activity} to {response.ResponseUri} ({Me.AccessBucket(bucketKey)})",
                                           response, request)
            If sentAsync Then
                Me.OnExceptionOccurred(ex)
            Else
                Throw ex
            End If
        Else
            Me.OnSendEventCompleted(response)
        End If
    End Sub

#End Region

#Region " TIME STAMP "

    ''' <summary> Gets or sets the epoch start time. </summary>
    ''' <value> The epoch start date. </value>
    Public Shared ReadOnly Property EpochStartTime As New DateTimeOffset(New DateTime(1970, 1, 1))

    ''' <summary> Converts a timestamp to an unix epoch time. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> The epoch. </returns>
    Public Shared Function ToUnixEpochTime(ByVal timestamp As DateTime) As Double
        Return 0.001 * timestamp.Subtract(BucketBase.EpochStartTime.DateTime).TotalMilliseconds
    End Function

    ''' <summary> Converts a timestamp to an unix epoch time. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> The epoch. </returns>
    Public Shared Function ToUnixEpochTime(ByVal timestamp As DateTimeOffset) As Double
        Return 0.001 * timestamp.Subtract(BucketBase.EpochStartTime).TotalMilliseconds
    End Function

    ''' <summary> Converts a timestamp to an ISO 8601. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> Timestamp as a String. </returns>
    Public Shared Function ToIso8601(ByVal timestamp As DateTime) As String
        Return timestamp.ToString("yyyy-MM-ddTHH:mm:ss.ffffZ")
    End Function

    ''' <summary> Converts a timestamp to an ISO 8601. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="timestamp"> The timestamp Date/Time. </param>
    ''' <returns> Timestamp as a String. </returns>
    Public Shared Function ToIso8601(ByVal timestamp As DateTimeOffset) As String
        Return timestamp.ToString("o")
    End Function

#End Region

End Class
