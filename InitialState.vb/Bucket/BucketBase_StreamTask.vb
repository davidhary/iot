' 
' Bucket Base Stream Task
' 
Imports System.Runtime.CompilerServices
Imports System.Windows.Threading
Imports isr.IoT.InitialState.DispatcherExtensions
Imports isr.IoT.InitialState.TaskExtensions
Imports System.Threading.Tasks

Partial Public Class BucketBase

#Region " TASK MANAGEMENT "

    ''' <summary> Reference to the awaiting task. </summary>
    ''' <value> The awaiting task. </value>
    Protected ReadOnly Property AwaitingTask As Threading.Tasks.Task

    ''' <summary> Reference to the Action task; this task status undergoes changes. </summary>
    ''' <value> The action task. </value>
    Protected ReadOnly Property ActionTask As Threading.Tasks.Task

    ''' <summary> Reference to the cancellation source. </summary>
    ''' <value> The task cancellation source. </value>
    Protected ReadOnly Property TaskCancellationSource As Threading.CancellationTokenSource

    ''' <summary> Queries if a task is active. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <returns> <c>true</c> if a task is active; otherwise <c>false</c> </returns>
    Public Function IsTaskActive() As Boolean
        Return Me.ActionTask.IsTaskActive
    End Function

    ''' <summary> Waits for the task to idle. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub AwaitTaskIdle(ByVal timeout As TimeSpan)
        If Me.ActionTask IsNot Nothing Then Me.ActionTask.Wait(timeout)
    End Sub

#End Region

#Region " STREAM TASK "

    ''' <summary> Stream Task canceled. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    Private Sub StreamTaskCanceled()
        Me.OnStreamTaskEnded(TaskStatus.Canceled)
    End Sub

    ''' <summary> Starts the action task. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="taskAction"> The action to stream the entities, which calls
    '''                           <see cref="StreamEvents(Of T)(IEnumerable(Of T), IEnumerable(Of Date), Integer, String)"/>. 
    ''' </param>
    ''' <returns> The awaiting task. </returns>
    Private Async Function AsyncAwaitTask(ByVal taskAction As Action) As Task
        Me._ActionTask = Task.Run(taskAction)
        Await Me.ActionTask '  Task.Run(streamEntitiesAction)
        Try
            Me.ActionTask?.Wait()
            Me.OnStreamTaskEnded(If(Me.ActionTask Is Nothing, TaskStatus.RanToCompletion, Me.ActionTask.Status))
        Catch ex As AggregateException
            Me.OnExceptionOccurred(ex)
        Finally
            Me.TaskCancellationSource.Dispose()
        End Try
    End Function

    ''' <summary> Starts Streaming the events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="bucketKey">            The bucket key. </param>
    ''' <param name="timeout">              The timeout. </param>
    ''' <param name="streamEntitiesAction"> The action to stream the entities, which calls
    '''                                     <see cref="StreamEvents(Of T)(IEnumerable(Of T), IEnumerable(Of Date), Integer, String)"/>. 
    ''' </param>
    Public Overridable Sub StartStreamEvents(ByVal bucketKey As String, ByVal timeout As TimeSpan, ByVal streamEntitiesAction As Action)
        If Me.IsTaskActive Then
            Throw New InvalidOperationException($"Stream task is {Me.ActionTask.Status}")
        Else
            Me._TaskCancellationSource = New Threading.CancellationTokenSource
            Me.TaskCancellationSource.Token.Register(AddressOf Me.StreamTaskCanceled)
            Me.TaskCancellationSource.CancelAfter(timeout)
            ' the action class is created withing the Async/Await function
            Me._AwaitingTask = Me.AsyncAwaitTask(streamEntitiesAction)
        End If
    End Sub

    ''' <summary> Event queue for all listeners interested in <see cref="StreamTaskEnded"/> events. </summary>
    Public Event StreamTaskEnded As EventHandler(Of TaskStatusEventArgs)

    ''' <summary> Raises the <see cref="StreamTaskEnded"/> event. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="status"> The task. </param>
    Protected Overridable Sub OnStreamTaskEnded(ByVal status As TaskStatus)
        Me.OnTaskEnded(New TaskStatusEventArgs(status))
    End Sub

    ''' <summary> Raises the <see cref="StreamTaskEnded"/> event. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="e"> Event information to Task to registered event handlers. </param>
    Protected Overridable Sub OnTaskEnded(ByVal e As TaskStatusEventArgs)
        Dim evt As EventHandler(Of TaskStatusEventArgs) = Me.StreamTaskEndedEvent
        evt?.Invoke(Me, e)
    End Sub

#End Region

#Region " STREAM ACTION "

    ''' <summary>
    ''' Streams a collection of entities using a multiple API calls each streaming a
    ''' <paramref name="bufferSize"/> size of entities.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="entities">   The entities. </param>
    ''' <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
    ''' <param name="bufferSize"> The buffer size. </param>
    ''' <param name="bucketKey">  The key of the bucket receiving the events. </param>
    Public Sub StreamEvents(Of T)(ByVal entities As IEnumerable(Of T), ByVal timestamps As IEnumerable(Of DateTime), ByVal bufferSize As Integer, ByVal bucketKey As String)
        Dim elementsQueue As New Queue(Of T)(entities)
        Dim timeStampsQueue As New Queue(Of DateTime)(timestamps)
        Do While elementsQueue.Any
            Try
                If Me.TaskCancellationSource IsNot Nothing AndAlso
                    Me.TaskCancellationSource.Token.IsCancellationRequested Then
                    Exit Do
                End If
                ' wait if rate limit was hit or if the remaining request count is zero
                Dispatcher.CurrentDispatcher.Wait(Me.RateLimit.PauseTimespan)
                Dispatcher.CurrentDispatcher.DoEvents
                Dim n As Integer = Math.Min(bufferSize, elementsQueue.Count)
                Dim elementList As New List(Of T)(elementsQueue.Take(n))
                Dim timeList As New List(Of DateTime)(timeStampsQueue.Take(n))
                Me.StreamEvents(Of T)(elementList, timeList, bucketKey, False)
                ' if no error, dequeue
                Do While n > 0
                    elementsQueue.Dequeue()
                    timeStampsQueue.Dequeue()
                    n -= 1
                Loop
                Dispatcher.CurrentDispatcher.DoEvents
            Catch ex As isr.IoT.InitialState.RestApiException
                If ex.StatusCode = isr.IoT.InitialState.RateLimit.RateLimitedRequestStatusCode AndAlso
                    Me.RateLimit.SuccessiveRateLimitCountOut > 0 Then
                    ' if request rate hit limit, try again unless the successive rate limit count out expired.
                Else
                    Me.TaskCancellationSource?.Cancel()
                    Me.OnExceptionOccurred(ex)
                    Exit Do
                End If
            End Try
        Loop
    End Sub

    ''' <summary>
    ''' Streams a collection of entities using a multiple API calls each streaming a
    ''' <paramref name="bufferSize"/> size of entities.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="entities">   The entities. </param>
    ''' <param name="timestamps"> The timestamps to associate with the events of each entity. </param>
    ''' <param name="bufferSize"> The buffer size. </param>
    ''' <param name="bucketKey">  The key of the bucket receiving the events. </param>
    Public Sub StreamEvents(Of T)(ByVal entities As IEnumerable(Of T), ByVal timestamps As IEnumerable(Of DateTimeOffset), ByVal bufferSize As Integer, ByVal bucketKey As String)
        Dim elementsQueue As New Queue(Of T)(entities)
        Dim timeStampsQueue As New Queue(Of DateTimeOffset)(timestamps)
        Do While elementsQueue.Any
            Try
                If Me.TaskCancellationSource IsNot Nothing AndAlso
                    Me.TaskCancellationSource.Token.IsCancellationRequested Then
                    Exit Do
                End If
                ' wait if rate limit was hit or if the remaining request count is zero
                Dispatcher.CurrentDispatcher.Wait(Me.RateLimit.PauseTimespan)
                Dispatcher.CurrentDispatcher.DoEvents
                Dim n As Integer = Math.Min(bufferSize, elementsQueue.Count)
                Dim elementList As New List(Of T)(elementsQueue.Take(n))
                Dim timeList As New List(Of DateTimeOffset)(timeStampsQueue.Take(n))
                Me.StreamEvents(Of T)(elementList, timeList, bucketKey, False)
                ' if no error, dequeue
                Do While n > 0
                    elementsQueue.Dequeue()
                    timeStampsQueue.Dequeue()
                    n -= 1
                Loop
                Dispatcher.CurrentDispatcher.DoEvents
            Catch ex As isr.IoT.InitialState.RestApiException
                If ex.StatusCode = isr.IoT.InitialState.RateLimit.RateLimitedRequestStatusCode AndAlso
                    Me.RateLimit.SuccessiveRateLimitCountOut > 0 Then
                    ' if request rate hit limit, try again unless the successive rate limit count out expired.
                Else
                    Me.TaskCancellationSource?.Cancel()
                    Me.OnExceptionOccurred(ex)
                    Exit Do
                End If
            End Try
        Loop
    End Sub

#End Region


End Class


