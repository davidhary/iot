Imports System.Reflection

Imports RestSharp
Imports RestSharp.Serialization.Json

''' <summary> An ISO 8601 event bucket. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/10/2019 </para>
''' </remarks>
Public Class Iso8601EventBucket
    Inherits BucketBase

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="accessKey"> The access key. </param>
    Public Sub New(ByVal accessKey As String)
        MyBase.New(accessKey)
    End Sub

    ''' <summary>
    ''' Enumerates the events to stream to the bucket as <see cref="Iso8601Event">events</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="entity">    The entity which properties are streamed to the bucket as
    '''                          <see cref="Iso8601Event">events</see>. </param>
    ''' <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the events in this collection.
    ''' </returns>
    Private Function EnumerateEvents(Of T)(ByVal entity As T, ByVal timestamp As DateTime) As IEnumerable(Of Iso8601Event)
        Dim epoch As String = BucketBase.ToIso8601(timestamp)
        Dim events As IEnumerable(Of Iso8601Event) = Nothing
        If GetType(T).IsAssignableFrom(GetType(IDictionary(Of String, String))) Then
            events = DirectCast(entity, IDictionary(Of String, String)).Select(Function(kvp) New Iso8601Event With {
                .iso8601 = epoch,
                .key = kvp.Key,
                .value = kvp.Value
            })
        Else
            Dim properties() As PropertyInfo = GetType(T).GetProperties()
            events = properties.Select(Function(prop) New Iso8601Event With {
                .iso8601 = epoch,
                .key = prop.Name,
                .value = prop.GetValue(entity).ToString()
            })
        End If
        Return events
    End Function

    ''' <summary> Stream events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="entities">   The entities. </param>
    ''' <param name="timestamps"> The timestamps. </param>
    ''' <param name="bucketKey">  The bucket key. </param>
    ''' <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
    '''                                                     response;
    '''                                                     otherwise, <c>false</c>. </param>
    Public Overrides Sub StreamEvents(Of T)(ByVal entities As IEnumerable(Of T), ByVal timestamps As IEnumerable(Of DateTime),
                                          ByVal bucketKey As String, ByVal sendAsync As Boolean)
        If entities Is Nothing OrElse Not entities.Any Then Throw New ArgumentNullException(NameOf(entities))
        If timestamps Is Nothing OrElse Not timestamps.Any Then Throw New ArgumentNullException(NameOf(timestamps))
        If entities.Count <> timestamps.Count Then Throw New ArgumentException(NameOf(entities),
                                                                               $"Number of {NameOf(entities)} {entities.Count} must match the number of {NameOf(timestamps)} {timestamps.Count}")
        Dim events As New List(Of Iso8601Event)
        Dim i As Integer = 0
        For Each obj As T In entities
            events.AddRange(Me.EnumerateEvents(Of T)(obj, timestamps(i)))
            i += 1
        Next
        Me.StreamEvents(events, bucketKey, sendAsync)
    End Sub

    ''' <summary> Stream events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="obj">       The object. </param>
    ''' <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
    ''' <param name="bucketKey"> The bucket key. </param>
    ''' <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
    '''                                                   response;
    '''                                                   otherwise, <c>false</c>. </param>
    Public Overrides Sub StreamEvents(Of T)(ByVal obj As T, ByVal timestamp As DateTime, ByVal bucketKey As String, ByVal sendAsync As Boolean)
        If obj Is Nothing Then Throw New ArgumentNullException(NameOf(obj))
        Dim epoch As String = BucketBase.ToIso8601(timestamp)
        Dim events As IEnumerable(Of Iso8601Event) = Nothing ' List(Of [Event]) = Nothing
        If GetType(T).IsAssignableFrom(GetType(IDictionary(Of String, String))) Then
            events = DirectCast(obj, IDictionary(Of String, String)).Select(Function(kvp) New Iso8601Event With {
                .iso8601 = epoch,
                .key = kvp.Key,
                .value = kvp.Value
            })
        Else
            Dim properties() As PropertyInfo = GetType(T).GetProperties()
            events = properties.Select(Function(prop) New Iso8601Event With {
                .iso8601 = epoch,
                .key = prop.Name,
                .value = prop.GetValue(obj).ToString()
            })
        End If
        Me.StreamEvents(events, bucketKey, sendAsync)
    End Sub

    ''' <summary>
    ''' Enumerates the events to stream to the bucket as <see cref="Iso8601Event">events</see>.
    ''' </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <param name="entity">    The entity which properties are streamed to the bucket as
    '''                          <see cref="Iso8601Event">events</see>. </param>
    ''' <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the events in this collection.
    ''' </returns>
    Private Function EnumerateEvents(Of T)(ByVal entity As T, ByVal timestamp As DateTimeOffset) As IEnumerable(Of Iso8601Event)
        Dim epoch As String = BucketBase.ToIso8601(timestamp)
        Dim events As IEnumerable(Of Iso8601Event) = Nothing
        If GetType(T).IsAssignableFrom(GetType(IDictionary(Of String, String))) Then
            events = DirectCast(entity, IDictionary(Of String, String)).Select(Function(kvp) New Iso8601Event With {
                .iso8601 = epoch,
                .key = kvp.Key,
                .value = kvp.Value
            })
        Else
            Dim properties() As PropertyInfo = GetType(T).GetProperties()
            events = properties.Select(Function(prop) New Iso8601Event With {
                .iso8601 = epoch,
                .key = prop.Name,
                .value = prop.GetValue(entity).ToString()
            })
        End If
        Return events
    End Function

    ''' <summary> Stream events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="entities">   The entities. </param>
    ''' <param name="timestamps"> The timestamps. </param>
    ''' <param name="bucketKey">  The bucket key. </param>
    ''' <param name="sendAsync">  <c>True</c> to stream the events without waiting for confirmed
    '''                                                     response;
    '''                                                     otherwise, <c>false</c>. </param>
    Public Overrides Sub StreamEvents(Of T)(ByVal entities As IEnumerable(Of T), ByVal timestamps As IEnumerable(Of DateTimeOffset),
                                          ByVal bucketKey As String, ByVal sendAsync As Boolean)
        If entities Is Nothing OrElse Not entities.Any Then Throw New ArgumentNullException(NameOf(entities))
        If timestamps Is Nothing OrElse Not timestamps.Any Then Throw New ArgumentNullException(NameOf(timestamps))
        If entities.Count <> timestamps.Count Then Throw New ArgumentException(NameOf(entities),
                                                                               $"Number of {NameOf(entities)} {entities.Count} must match the number of {NameOf(timestamps)} {timestamps.Count}")
        Dim events As New List(Of Iso8601Event)
        Dim i As Integer = 0
        For Each obj As T In entities
            events.AddRange(Me.EnumerateEvents(Of T)(obj, timestamps(i)))
            i += 1
        Next
        Me.StreamEvents(events, bucketKey, sendAsync)
    End Sub

    ''' <summary> Stream events. </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="obj">       The object. </param>
    ''' <param name="timestamp"> The timestamp to use for all of the events of this entity. </param>
    ''' <param name="bucketKey"> The bucket key. </param>
    ''' <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
    '''                                                   response;
    '''                                                   otherwise, <c>false</c>. </param>
    Public Overrides Sub StreamEvents(Of T)(ByVal obj As T, ByVal timestamp As DateTimeOffset, ByVal bucketKey As String, ByVal sendAsync As Boolean)
        If obj Is Nothing Then Throw New ArgumentNullException(NameOf(obj))
        Dim epoch As String = BucketBase.ToIso8601(timestamp)
        Dim events As IEnumerable(Of Iso8601Event) = Nothing ' List(Of [Event]) = Nothing
        If GetType(T).IsAssignableFrom(GetType(IDictionary(Of String, String))) Then
            events = DirectCast(obj, IDictionary(Of String, String)).Select(Function(kvp) New Iso8601Event With {
                .iso8601 = epoch,
                .key = kvp.Key,
                .value = kvp.Value
            })
        Else
            Dim properties() As PropertyInfo = GetType(T).GetProperties()
            events = properties.Select(Function(prop) New Iso8601Event With {
                .iso8601 = epoch,
                .key = prop.Name,
                .value = prop.GetValue(obj).ToString()
            })
        End If
        Me.StreamEvents(events, bucketKey, sendAsync)
    End Sub

    ''' <summary> Streams <see cref="Iso8601Event">epoch events</see> </summary>
    ''' <remarks> David, 10/27/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="events">    The events. </param>
    ''' <param name="bucketKey"> The bucket key. </param>
    ''' <param name="sendAsync"> <c>True</c> to stream the events without waiting for confirmed
    '''                          response; otherwise, <c>false</c>. </param>
    Friend Overloads Sub StreamEvents(ByVal events As IEnumerable(Of Iso8601Event), ByVal bucketKey As String, ByVal sendAsync As Boolean)
        If events Is Nothing OrElse Not events.Any Then Throw New ArgumentNullException(NameOf(events))
        If String.IsNullOrEmpty(bucketKey) Then Throw New ArgumentException("a bucket key is required")
        Dim activity As String = String.Empty
        Dim request As New RestRequest(Client.EventsEndPoint, Method.POST)
        request.AddHeader(Client.AccessKeyHeader, Me.AccessKey)
        request.AddHeader(Client.BucketKeyHeader, bucketKey)
        request.AddHeader(Client.AcceptVersionHeader, Client.ApiVersion)
        request.JsonSerializer = New JsonSerializer()
        request.AddJsonBody(events)
        activity = $"streaming {Client.EventsEndPoint}"
        If sendAsync Then
            Me.RestClient.ExecuteAsync(request, Sub(response)
                                                    Me.OnSendEventCompleted(activity, bucketKey, response, request, sendAsync)
                                                End Sub)
        Else
            Dim response As IRestResponse = Me.RestClient.Execute(request)
            Me.OnSendEventCompleted(activity, bucketKey, response, request, sendAsync)
        End If
    End Sub

End Class

