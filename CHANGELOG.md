# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.0.7612] - 2010-11-03
* converted to C#

## [0.1.7068] - 2019-05-09
* Built upon base information from the Tek code and other online resources.

\(C\) 2019 Integrated Scientific Resources, Inc.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
